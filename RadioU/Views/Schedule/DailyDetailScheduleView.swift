//
//  DailyDetailScheduleView.swift
//  RadioU
//
//  Created by Jake on 4/14/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit

class DailyDetailScheduleView: UIScrollView {

    var weekday: Int!
    var timeLabels = [UILabel]()
    weak var scheduleEntryDelegate: ScheduleEntryDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupTimer()
    }
    
    override func removeFromSuperview() {
        NotificationCenter.default.removeObserver(self, name: .minuteChange, object: nil)
        super.removeFromSuperview()
    }
    
    func setup(with station: Station, weekday: Int, scheduleEntryDelegate: ScheduleEntryDelegate?) {
        self.clipsToBounds = false
        self.weekday = weekday
        self.scheduleEntryDelegate = scheduleEntryDelegate
        
        let labelBuffer: CGFloat = 35
        let colorList: [UIColor] = [.lightGreen, .teal, .lightRed, .clementine, .magenta, .purple, .navy]
        let hours = ["12:00AM", "1:00AM", "2:00AM", "3:00AM", "4:00AM", "5:00AM", "6:00AM", "7:00AM", "8:00AM", "9:00AM", "10:00AM", "11:00AM", "12:00PM", "1:00PM", "2:00PM", "3:00PM", "4:00PM", "5:00PM", "6:00PM", "7:00PM", "8:00PM", "9:00PM", "10:00PM", "11:00PM"]

        var colorIndex = -1
        var previousShowId = -1
        
        for hour in 0...hours.count - 1 {
            let timeLabel = UILabel(frame: CGRect(x: 3, y: CGFloat(hour)*labelBuffer, width: 45, height: 17))
            timeLabel.text = hours[hour]
            timeLabel.font = UIFont.systemFont(ofSize: 10)
            timeLabel.textColor = .lightGray
            self.addSubview(timeLabel)
            timeLabels.append(timeLabel)

            let underline = UIView(frame: CGRect(x: 3, y: timeLabel.frame.origin.y + labelBuffer - 2 - timeLabel.frame.height/2, width: self.frame.width - 6, height: 2))
            underline.layer.cornerRadius = 4
            underline.backgroundColor = .bgGray
            self.addSubview(underline)
            
            let entrySectionWidth = underline.frame.width - timeLabel.frame.width - 6
            
            let showIds = station.getSchedule(at: weekday, hour: hour)
            
            for id in showIds {
                guard let show = station.shows[id] else { return }
            
                if (id != previousShowId) {
                    colorIndex += 1
                    if (colorIndex >= colorList.count) {
                        colorIndex = 0
                    }
                }
                
                previousShowId = show.id
                
                var showTime: Time!
                
                if (show.showTimes.count == 1) {
                    showTime = show.showTimes[0]
                } else {
                    for time in show.showTimes {
                        if (time.contains(weekday: weekday, hour: hour)) {
                            showTime = time
                            break
                        }
                    }
                }
                
                let startComponents = Calendar.current.dateComponents([.weekday, .hour, .minute], from: showTime.localizedStartTime)
                let endComponents = Calendar.current.dateComponents([.weekday, .hour, .minute], from: showTime.localizedEndTime)
                
                var startMinutes = 0
                if (startComponents.weekday! == weekday &&
                    startComponents.hour! == hour) {
                    startMinutes = startComponents.minute!
                }
                
                var endMinutes = 59
                if (endComponents.weekday! == weekday &&
                    endComponents.hour! == hour) {
                    endMinutes = endComponents.minute!
                }
                
                let minutes = endMinutes - startMinutes + 1
                let entryWidth = entrySectionWidth/(60/CGFloat(minutes))

                let xPosition = entrySectionWidth/(60/CGFloat(startMinutes))

                let frame = CGRect(x: timeLabel.frame.width + timeLabel.frame.origin.x + xPosition, y: timeLabel.frame.origin.y - 5, width: entryWidth - 1 , height: 25)
                let entry = ScheduleEntry(frame: frame, color: colorList[colorIndex], show: show)
                entry.delegate = scheduleEntryDelegate
                self.addSubview(entry)
            }
        }
        
        updateHourHighlight()
    }

    func updateHourHighlight() {
        let components = Calendar.current.dateComponents([.weekday, .hour], from: Date())
        if (components.weekday! == weekday) {
            for hour in 0...timeLabels.count - 1 {
                if (hour == components.hour!) {
                    timeLabels[hour].textColor = .lightRed
                    timeLabels[hour].font = UIFont.systemFont(ofSize: 9, weight: 1000)
                } else {
                    timeLabels[hour].textColor = .lightGray
                    timeLabels[hour].font = UIFont.systemFont(ofSize: 10)
                }
            }
        } else {
            timeLabels[23].textColor = .lightGray // If its not the same day then the last hour is the only one that could be on
        }
    }
    
    private func setupTimer() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateHourHighlight), name: .minuteChange, object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
