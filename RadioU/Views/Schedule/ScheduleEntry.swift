//
//  ScheduleShowView.swift
//  RadioU
//
//  Created by Jake on 4/14/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import MarqueeLabel

protocol ScheduleEntryDelegate: class {
    func isExpanded() -> Bool
    func transitionWith(show: Show)
}

class ScheduleEntry: UIView {

    weak var show: Show!
    weak var delegate: ScheduleEntryDelegate?
    
    init(frame: CGRect, color: UIColor, show: Show) {
        super.init(frame: frame)
        self.show = show
        self.backgroundColor = color
        self.layer.cornerRadius = 4
        let tap = UITapGestureRecognizer(target: self, action: #selector(showViewTap))
        self.addGestureRecognizer(tap)

        let showLabel = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        showLabel.text = show.name
        showLabel.textColor = .white
        showLabel.font = UIFont(name: "AvenirNext-Medium", size: 14)
        showLabel.textAlignment = .center
        showLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        
        self.addSubview(showLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func showViewTap() {
        delegate!.transitionWith(show: show)
    }
    
}
