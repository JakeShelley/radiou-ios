//
//  CalendarContainerView.swift
//  RadioU
//
//  Created by Jake on 4/13/17.
//  Copyright © 2017 Jake. All rights reserved.
//
// REFACTOR NOTE: When I built this initially I thought the the .weekday value of Saturday was 0 not 7 so I fixed it in a stupid way

import UIKit
import RxCocoa
import RxSwift

class ScheduleView: UIView {

    let disposeBag = DisposeBag()
    
    var screenWidth: CGFloat!
    var topBar: UIView!
    var scheduleContainer: UIView!
    var scheduleXPosition: CGFloat!
    var station: Station!
    lazy var dayButtonList = [UIButton]()
    weak var scheduleEntryDelegate: ScheduleEntryDelegate?
    
    init(frame: CGRect, station: Station, scheduleEntryDelegate: ScheduleEntryDelegate?) {
        super.init(frame: frame)
        screenWidth = frame.width
        self.station = station
        self.scheduleEntryDelegate = scheduleEntryDelegate
        
        var weekday = Calendar.current.component(.weekday, from: Date()) // Get initial position from the day
        if (weekday == 7) {
            weekday = 0
        }
        
        scheduleXPosition = screenWidth*CGFloat(weekday)
        
        setupTopBar()
        setupScheduleContainer()
        addSwipeGesture()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    func setupTopBar() {
        let buttonWidth = screenWidth/7
        let colorList: [UIColor] = [.lightGreen, .teal, .lightRed, .clementine, .magenta, .purple, .navy]
        let dayList = ["Sat", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri"]
        dayButtonList = dayList.enumerated().map({ (index, day) -> UIButton in
            let dayButton = UIButton(frame: CGRect(x: buttonWidth*CGFloat(index), y: 4, width: buttonWidth, height: 45))
            dayButton.setTitle(day, for: .normal)
            dayButton.setTitleColor(.lightGray, for: .normal)
            dayButton.setTitleColor(colorList[index], for: .selected)
            dayButton.titleLabel?.font = UIFont(name: "AvenirNext-Medium", size: 15)
            dayButton.addTarget(self, action: #selector(dayButtonPress(sender:)), for: .touchUpInside)
            dayButton.tag = index
            return dayButton
        })
        
        for dayButton in dayButtonList {
            self.addSubview(dayButton)
        }
        
        let underline = UIView(frame: CGRect(x: 0, y: 47, width: self.frame.width, height: 2))
        underline.backgroundColor = .bgGray
        self.addSubview(underline)
    }
    
    func setupScheduleContainer() {
        scheduleContainer = UIView(frame: CGRect(x: -scheduleXPosition, y: 60, width: screenWidth*7, height: 832))
        scheduleContainer.rx.observe(CGRect.self, "frame")
            .asObservable()
            .subscribe(onNext: {[weak self] frame in
                var index = Int(-frame!.origin.x/self!.screenWidth)
                if (index == 7) {
                    index = 0
                }
                
                self?.dayButtonList[index].isSelected = true
            })
            .addDisposableTo(disposeBag)
        
        for weekday in 1...7  {
            let dailyDetailView = DailyDetailScheduleView(frame: CGRect(x: screenWidth*CGFloat(weekday), y: 0, width: screenWidth, height: scheduleContainer.frame.height))
            
            if (weekday == 7) {
                dailyDetailView.frame.origin.x = 0
            }
            
            dailyDetailView.setup(with: station, weekday: weekday, scheduleEntryDelegate: scheduleEntryDelegate)
            scheduleContainer.addSubview(dailyDetailView)
        }
        
        self.addSubview(scheduleContainer)
    }
    
    func addSwipeGesture() {
        let rightSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeDay))
        rightSwipeGesture.direction = .right
        self.addGestureRecognizer(rightSwipeGesture)
        
        let leftSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeDay))
        leftSwipeGesture.direction = .left
        self.addGestureRecognizer(leftSwipeGesture)
    }
    
    func swipeDay(gesture: UISwipeGestureRecognizer) {
        if (!scheduleEntryDelegate!.isExpanded()) {
            return
        }
        
        var index = Int(-self.scheduleContainer.frame.origin.x/screenWidth)
        if (index == 7) {
            index = 0
        }
        
        if (gesture.direction == .right) {
            if (index == 0) { return }
            performTransition(index - 1)
        } else if (gesture.direction == .left) {
            if (index == 6) { return }
            performTransition(index + 1)
        }
    }
    
    func dayButtonPress(sender: UIButton) {
        performTransition(sender.tag)
    } 
    
    func performTransition(_ index: Int) {
        scheduleXPosition = screenWidth*CGFloat(index)
        resetButtons()
        UIView.setAnimationCurve(.easeOut)
        UIView.animate(withDuration: 0.2, animations: {
            self.scheduleContainer.frame.origin.x = -self.scheduleXPosition
        })
    }
    
    func transitionToCurrentWeekday() {
        var weekday = Calendar.current.component(.weekday, from: Date()) // Get initial position from the day
        if (weekday == 7) {
            weekday = 0
        }
        
        performTransition(weekday)
    }

    func resetButtons() {
        for button in dayButtonList {
            button.isSelected = false
        }
    }
    
}
