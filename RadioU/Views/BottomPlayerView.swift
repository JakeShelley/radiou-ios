//
//  BottomPlayerView.swift
//  RadioU
//
//  Created by Jake on 4/28/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MarqueeLabel
import RealmSwift
import RxRealm

protocol BottomPlayerDelegate: class {
    func presentLoginView(with show: Show)
    func snapBottomPlayer(fullScreen: Bool, view: BottomPlayerView)
}

class BottomPlayerView: UIView {

    @IBOutlet weak var topBarCurrentShowLabel: MarqueeLabel!
    @IBOutlet weak var topBarStationNameLabel: MarqueeLabel!
    @IBOutlet weak var topBarPlayButton: UIButton!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var upImage: UIImageView!
    @IBOutlet weak var imageShadow: UIImageView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var tagLabel: MarqueeLabel!
    @IBOutlet weak var showLabel: MarqueeLabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var topBarEventReceiver: UIView!
    @IBOutlet weak var timeLabel: UILabel!

    var station: Station!
    weak var delegate: BottomPlayerDelegate!

    private let disposeBag = DisposeBag()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setTimer()
        setupRx()
        setupFavoriting()
    }
    
    override func removeFromSuperview() {
        NotificationCenter.default.removeObserver(self, name: .minuteChange, object: nil)
        super.removeFromSuperview()
    }
    
    func playPause() {
        Player.sharedInstance.playStation(station)
    }
    
    func performSetup(station: Station) {
        self.station = station
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(topBarPressed))
        topBarEventReceiver.addGestureRecognizer(tap)
        topBarPlayButton.addTarget(self, action: #selector(playPause), for: .touchUpInside)
        let lineBuffer = UIView(frame: CGRect(x: 0, y: -1, width: UIScreen.main.bounds.width, height: 1))
        lineBuffer.backgroundColor = .bgGray
        self.addSubview(lineBuffer)
        
        upImage.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
        playButton.layer.shadowColor = UIColor.lightGray.cgColor
        
        setupImage()
        setupLabels()
        setupFavoriteButton()
    }
    
    func topBarPressed() {
        delegate.snapBottomPlayer(fullScreen: self.frame.origin.y != 0, view: self)
    }
    
    func setTopBarAlpha(_ alpha: CGFloat) {
        topBar.alpha = alpha
    }
    
    func updateAtMinuteChange() {
        setText()
        setupFavoriteButton()
    }
    
    func setText() {
        topBarCurrentShowLabel.text = station.currentShow.name
        topBarStationNameLabel.text = station.name
        showLabel.text = station.currentShow.name
        tagLabel.text = station.currentShow.tagString
        timeLabel.text = station.currentShow.timeLabelString
    }
      
    private func setTimer() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateAtMinuteChange), name: .minuteChange, object: nil)
    }

    private func setupRx() {
        let player = Player.sharedInstance
        player.isPlaying.asObservable()
            .skip(1)
            .subscribe(onNext: {[weak self] isPlaying in
                self?.setPlayButtonImage(isPlaying: isPlaying)
            })
            .addDisposableTo(disposeBag)
    }
    
    private func setPlayButtonImage(isPlaying: Bool) {
        if (isPlaying) {
            playButton.setImage(#imageLiteral(resourceName: "pause-xl"), for: .normal)
            topBarPlayButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
        } else {
            playButton.setImage(#imageLiteral(resourceName: "play-xl"), for: .normal)
            topBarPlayButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        }
    }
    
    private func setupImage() {
        imageShadow.layer.shadowOpacity = 0.6
        imageShadow.layer.shadowColor = UIColor.lightGray.cgColor
        imageShadow.layer.shadowRadius = 5
        imageShadow.layer.shadowOffset = CGSize(width: 0, height: 8)
        
        iconImage.layer.cornerRadius = 5
        iconImage.clipsToBounds = true
        iconImage.alpha = 0
        iconImage.backgroundColor = .bgGray
        iconImage.sd_setImage(with: URL(string: station.iconPath), completed: {(image, error, cacheType, url) -> Void in
            if (cacheType == .none) {
                self.iconImage.alpha = 0
                UIView.animate(withDuration: 0.15, animations: {
                    self.iconImage.alpha = 1
                }, completion: nil)
            } else {
                self.iconImage.alpha = 1
            }
        })
    }
    
    private func setupLabels() {
        topBarCurrentShowLabel.fadeLength = MARQUEE_FADE_LENGTH
        topBarCurrentShowLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        topBarStationNameLabel.fadeLength = MARQUEE_FADE_LENGTH
        topBarStationNameLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        showLabel.fadeLength = MARQUEE_FADE_LENGTH
        showLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        tagLabel.fadeLength = MARQUEE_FADE_LENGTH
        tagLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        setText()
    }
    
    private func setupFavoriteButton() {
        if (station.currentShow.name == "Heavy Rotation") {
            favoriteButton.isUserInteractionEnabled = false
            favoriteButton.isHidden = true
        } else {
            let realm = try! Realm()
            if (realm.object(ofType: Show.self, forPrimaryKey: station.currentShow.id) != nil) {
                favoriteButton.tintColor = .lightRed
            } else {
                favoriteButton.tintColor = .bgGray
            }
            
            favoriteButton.isUserInteractionEnabled = true
            favoriteButton.isHidden = false
        }
    }
    
    private func setupFavoriting() {
        let realm = try! Realm()
        Observable.collection(from: realm.objects(Show.self))
            .skip(1)
            .subscribe({[weak self] _ in
                if (realm.object(ofType: Show.self, forPrimaryKey: self?.station.currentShow.id) != nil) {
                    self?.favoriteButton.tintColor = .lightRed
                } else {
                    self?.favoriteButton.tintColor = .bgGray
                }
            })
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func favoriteButtonPressed(_ sender: Any) {
        let realm = try! Realm()
        if (realm.objects(User.self).count == 0) {
            delegate.presentLoginView(with: station.currentShow)
        } else {
            handleFavorite(show: station.currentShow)
        }
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
        playPause()
    }
    
}
