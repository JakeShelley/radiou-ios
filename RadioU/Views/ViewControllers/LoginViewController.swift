//
//  LoginViewController.swift
//  RadioU
//
//  Created by Jake on 3/11/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import RxSwift
import RxCocoa

protocol LoginViewControllerDelegate: class {
    func loginShouldClose()
}

class LoginViewController: UIViewController {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var switcherView: UIView!
    @IBOutlet weak var topContainer: UIView!
    @IBOutlet weak var background: UIImageView!
    
    let viewModel = LoginViewModel()
    private let disposeBag = DisposeBag()
    
    var messageText: String!
    var bottomContainer: UIView!
    var switcherBar: UIView!
    var joinLabel: UILabel!
    var loginLabel: UILabel!
    var registerUsernameField: FloatingTextField!
    var registerPasswordField: FloatingTextField!
    var registerButton: EnterCredentialButton!
    var usernameField: FloatingTextField!
    var passwordField: FloatingTextField!
    var loginButton: EnterCredentialButton!
    var activityIndicator: UIActivityIndicatorView!
    weak var delegate: LoginViewControllerDelegate?
    weak var showToFavorite: Show?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (messageText != nil) {
            messageLabel.text = messageText
        }
        
        setupSwitcherView()
        setupBottomContainer()
        setupActivityIndicator()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
        setupRx()
        applyDismissKeyboardGesture()
    }
    
    // Setup any RxSwift Observers
    func setupRx() {
        // Login
        usernameField.rx.text.orEmpty
            .bind(to: viewModel.username)
            .addDisposableTo(disposeBag)
        
        passwordField.rx.text.orEmpty
            .bind(to: viewModel.password)
            .addDisposableTo(disposeBag)
        
        viewModel.isTextValid
            .map { [weak self] in
                // TODO - Find better way to change UIButton background color on disable
                self?.loginButton.setBackgroundColor(enabled: $0)
                return $0
            }
            .bind(to: loginButton.rx.isEnabled)
            .addDisposableTo(disposeBag)
        
        loginButton.rx.tap
            .subscribe({[weak self] _ in
                self?.dismissKeyboard()
                self?.activityIndicator.startAnimating()
                self?.viewModel.loginUser(completion: { (statusCode, message) in
                    self?.activityIndicator.stopAnimating()
                    if (statusCode == 200) {
                        guard self?.delegate != nil else {
                            self?.closeView()
                            return
                        }
                        
                        self?.delegate?.loginShouldClose()
                    } else {
                        self?.showAlert(message: message)
                    }
                })
            })
            .addDisposableTo(disposeBag)
        
        // Registration
        
        registerUsernameField.rx.text.orEmpty
            .bind(to: viewModel.registerUsername)
            .addDisposableTo(disposeBag)
        
        registerPasswordField.rx.text.orEmpty
            .bind(to: viewModel.registerPassword)
            .addDisposableTo(disposeBag)
        
        
        viewModel.isRegistrationTextValid
            .map {[weak self] in
                // TODO - Find better way to change UIButton background color on disable
                self?.registerButton.setBackgroundColor(enabled: $0)
                return $0
            }
            .bind(to: registerButton.rx.isEnabled)
            .addDisposableTo(disposeBag)
        
        registerButton.rx.tap
            .subscribe({[weak self] _ in
                self?.dismissKeyboard()
                self?.activityIndicator.startAnimating()
                self?.viewModel.registerUser(completion: { (statusCode, message) in
                    self?.activityIndicator.stopAnimating()
                    if (statusCode == 201) {
                        guard self?.delegate != nil else {
                            self?.closeView()
                            return
                        }
                        
                        self?.delegate?.loginShouldClose()
                    } else {
                        self?.showAlert(message: message)
                    }
                })
            })
            .addDisposableTo(disposeBag)
    }

    // Mark: Programmatic View Setup
    
    // Setup activityIndicator
    func setupActivityIndicator() {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: self.view.frame.width/2 - 30, y: self.view.frame.height/2 - 30, width: 60, height: 60))
        activityIndicator.layer.cornerRadius = 5
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
    }
    
    // Setup switcherView
    func setupSwitcherView() {
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.width/2, height: switcherView.frame.height)
        
        // Register Label
        joinLabel = UILabel(frame: frame)
        joinLabel.isUserInteractionEnabled = true
        joinLabel.text = "JOIN RADIOU"
        joinLabel.textColor = .lightRed
        joinLabel.textAlignment = .center
        joinLabel.font = UIFont(name: "AvenirNext-Medium", size: 15)
        joinLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(joinSwitchAction)))
        switcherView.addSubview(joinLabel)
        
        // Login Label
        loginLabel = UILabel(frame: frame)
        loginLabel.isUserInteractionEnabled = true
        loginLabel.frame.origin.x = joinLabel.frame.width
        loginLabel.text = "LOGIN"
        loginLabel.textColor = .gray
        loginLabel.textAlignment = .center
        loginLabel.font = UIFont(name: "AvenirNext-Medium", size: 15)
        loginLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(loginSwitchAction)))
        switcherView.addSubview(loginLabel)
        
        // Bottom line
        let bottomLine = UIView(frame: frame)
        bottomLine.frame.size.height = 1
        bottomLine.frame.size.width = self.view.frame.width
        bottomLine.frame.origin.y = switcherView.frame.height - 1
        bottomLine.backgroundColor = .bgGray
        switcherView.addSubview(bottomLine)
        
        // Bottom switcher indicator
        switcherBar = UIView(frame: frame)
        switcherBar.frame.size.height = 3
        switcherBar.frame.origin.y = switcherView.frame.height - 3
        switcherBar.backgroundColor = .lightRed
        switcherView.addSubview(switcherBar)
    }
    
    // Setup Bottom Container
    func setupBottomContainer() {
        let BOTTOM_CONTAINER_HEIGHT: CGFloat = 225
        bottomContainer = UIView(frame: CGRect(x: 0, y: self.view.frame.height - BOTTOM_CONTAINER_HEIGHT, width: self.view.frame.width*2, height: BOTTOM_CONTAINER_HEIGHT))
        let sideBuffer: CGFloat = 20
        let itemHeight: CGFloat = 45
        let verticalBuffer: CGFloat = 15
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(joinSwitchAction))
        rightSwipe.direction = .right
        rightSwipe.delegate = self
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(loginSwitchAction))
        leftSwipe.direction = .left
        leftSwipe.delegate = self
        bottomContainer.addGestureRecognizer(rightSwipe)
        bottomContainer.addGestureRecognizer(leftSwipe)
        
        // First Half
        
        // RegisterUsernameField
        registerUsernameField = FloatingTextField(frame: CGRect(x: sideBuffer, y: 8, width: self.view.frame.width - sideBuffer*2, height: itemHeight),
                                                  title: "Username",
                                                  placeholder: "Register Username",
                                                  color: .lightRed)
        registerUsernameField.delegate = self
        bottomContainer.addSubview(registerUsernameField)
        
        // RegisterPasswordField
        registerPasswordField = FloatingTextField(frame: CGRect(x: sideBuffer, y: registerUsernameField.frame.origin.y + itemHeight + verticalBuffer, width: self.view.frame.width - sideBuffer*2, height: itemHeight),
                                                  title: "Password",
                                                  placeholder: "Register Password",
                                                  color: .lightRed)
        registerPasswordField.delegate = self
        registerPasswordField.isSecureTextEntry = true
        registerPasswordField.textContentType = .nickname
        bottomContainer.addSubview(registerPasswordField)
        
        // RegisterButton
        registerButton = EnterCredentialButton(frame: CGRect(x: sideBuffer, y: registerPasswordField.frame.origin.y + itemHeight + verticalBuffer + 8, width: self.view.frame.width - sideBuffer*2, height: itemHeight),
                                               title: "REGISTER",
                                               color: .lightRed)
        bottomContainer.addSubview(registerButton)
        
        // Second Half
        let dist = self.view.frame.width
        
        // UsernameField
        usernameField = FloatingTextField(frame: registerUsernameField.frame,
                                                  title: "Username",
                                                  placeholder: "Enter Username",
                                                  color: .lightRed)
        usernameField.frame.origin.x += dist
        usernameField.delegate = self
        bottomContainer.addSubview(usernameField)
        
        // PasswordField
        passwordField = FloatingTextField(frame: registerPasswordField.frame,
                                                  title: "Password",
                                                  placeholder: "Enter Password",
                                                  color: .lightRed)
        passwordField.frame.origin.x += dist
        passwordField.delegate = self
        passwordField.textContentType = .nickname
        passwordField.isSecureTextEntry = true
        bottomContainer.addSubview(passwordField)
        
        // RegisterButton
        loginButton = EnterCredentialButton(frame: registerButton.frame,
                                               title: "LOGIN",
                                               color: .lightRed)
        loginButton.frame.origin.x += dist
        bottomContainer.addSubview(loginButton)
        
        self.view.addSubview(bottomContainer)
        
        let notRightNowButton = UIButton(frame: CGRect(x: (self.view.frame.width - 100)/2, y: bottomContainer.frame.origin.y + loginButton.frame.origin.y + itemHeight + 8, width: 105, height: 30))
        notRightNowButton.setTitle("Not right now", for: .normal)
        notRightNowButton.setTitleColor(.lightRed, for: .normal)
        notRightNowButton.titleLabel!.font = UIFont.systemFont(ofSize: 14)
        notRightNowButton.addTarget(self, action: #selector(closeView), for: .touchUpInside)
        self.view.addSubview(notRightNowButton)
        
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.setAppearance()
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // Switch to join section
    func joinSwitchAction(sender: AnyObject?) {
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseOut, animations: {
            self.switcherBar.frame.origin.x = 0
            self.bottomContainer.frame.origin.x = 0
        }, completion: {_ in
            self.joinLabel.textColor = .lightRed
            self.loginLabel.textColor = .gray
        })
    }

    // Switch to login section
    func loginSwitchAction(sender: AnyObject?) {
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseOut, animations: {
            self.switcherBar.frame.origin.x = self.view.frame.width/2
            self.bottomContainer.frame.origin.x = -self.view.frame.width
        }, completion: {_ in
            self.loginLabel.textColor = .lightRed
            self.joinLabel.textColor = .gray
        })
    }

    // Mark: Keyboard
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }

    func applyDismissKeyboardGesture() {
        let dismissKeyboardGesture = UISwipeGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        dismissKeyboardGesture.direction = .down
        dismissKeyboardGesture.delegate = self
        self.view.addGestureRecognizer(dismissKeyboardGesture)
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    // Mark: Navigation
    
    func closeView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismissButtonPressed(_ sender: Any) {
        closeView()
    }
    
}

// TextField Delegate
extension LoginViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return viewModel.isValidCharacter(string: string)
    }

}

// Gesture Delegate
extension LoginViewController: UIGestureRecognizerDelegate {

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

}
