//
//  ChatViewController.swift
//  RadioU
//
//  Created by Jake Shelley on 9/4/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import Firebase
import JSQMessagesViewController
import RealmSwift

class ChatViewController: JSQMessagesViewController {

    var stationRef: DatabaseReference?
    var stationId: Int!
    var stationName: String? {
        didSet {
            title = stationName
        }
    }
    
    var messages = [JSQMessage]()
    
    var loadEarlierMessageRefHandle: DatabaseHandle?
    var newMessageRefHandle: DatabaseHandle?
    var loadEarlierMessagesSpinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    lazy var messageRef: DatabaseReference = self.stationRef!.child("messages")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .bgGray
        UIApplication.shared.statusBarStyle = .default
        
        let realm = try! Realm()
        
        loadEarlierMessagesSpinner.frame = CGRect(x: self.view.frame.width/2 + 10, y: 0, width: 20, height: 20)
        loadEarlierMessagesSpinner.tag = 1
        
        self.inputToolbar.contentView.leftBarButtonItem = nil
        observeMessages()
        senderId = String(realm.objects(User.self)[0].id)
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        showLoadEarlierMessagesHeader = true
    }
    
    deinit {
        if let refHandle = newMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
        
        if let refHandle = loadEarlierMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        
        cell.textView.layer.cornerRadius = 0
        
        if message.senderId == senderId {
            cell.textView?.textColor = UIColor.white
        } else {
            cell.textView?.textColor = UIColor.black
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        let itemRef = messageRef.childByAutoId()
        let messageItem = [
            "senderId": senderId!,
            "senderName": senderDisplayName!,
            "text": text!,
            "date": Date().timeIntervalSince1970,
            ] as [String : Any]
        itemRef.setValue(messageItem)
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        finishSendingMessage()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString? {
        let message = messages[indexPath.item]
        guard let senderDisplayName = message.senderDisplayName else {
            return NSAttributedString(string: "")
        }
        
        return NSAttributedString(string: senderDisplayName)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.item]
        guard let date = message.date else {
            return NSAttributedString(string: "")
        }
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.defaultDate = Date()
        
        let dateComponents = Calendar.current.dateComponents([.day, .year, .month], from: date)
        let currentComponents = Calendar.current.dateComponents([.day, .year, .month], from: Date())
        
        if (dateComponents.day != currentComponents.day ||
            dateComponents.month != currentComponents.month ||
            dateComponents.year != currentComponents.year) {
            formatter.dateFormat = "MMM d, yyyy"
        } else {
            formatter.dateFormat = "EEE h:mm a"
        }
        
        
        return NSAttributedString(string: formatter.string(from: date))
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        return 17
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, header headerView: JSQMessagesLoadEarlierHeaderView!, didTapLoadEarlierMessagesButton sender: UIButton!) {
        
        if (headerView.viewWithTag(1) == nil) {
            headerView.addSubview(loadEarlierMessagesSpinner)
        }
        
        headerView.loadButton.isHidden = true
        loadEarlierMessagesSpinner.startAnimating()
        
        messageRef = stationRef!.child("messages")
        guard let latestMessage = messages.first else {
            observeMessages()
            return
        }
        
        let messageQuery = messageRef.queryOrdered(byChild: "date").queryEnding(atValue: latestMessage.date.timeIntervalSince1970 - 0.001 as NSNumber).queryLimited(toLast: 20)
        loadEarlierMessageRefHandle = messageQuery.observe(.value, with: { [weak self] (snapshot) -> Void in
            
            guard let messageData = snapshot.value as? Dictionary<String, Any> else {
                headerView.loadButton.isHidden = false
                self!.loadEarlierMessagesSpinner.stopAnimating()
                return
            }
            
            let newMessages = messageData.map({ (key, value) -> JSQMessage in
                let messageData = value as! Dictionary<String, Any>
                return JSQMessage(senderId: messageData["senderId"] as! String!,
                                  senderDisplayName: messageData["senderName"] as! String!,
                                  date: Date(timeIntervalSince1970: messageData["date"] as! Double!),
                                  text: messageData["text"] as! String!)
            })

            let sortedNewMessages = newMessages.sorted {
                return $0.date.timeIntervalSince1970 <= $1.date.timeIntervalSince1970
            }
            
            self!.messages = sortedNewMessages + self!.messages
            self!.finishSendingMessage(animated: false)
            self!.scroll(to: [0, 0], animated: false)
            
            headerView.loadButton.isHidden = false
            self!.loadEarlierMessagesSpinner.stopAnimating()
        })
    }
    
    private func observeMessages() {
        messageRef = stationRef!.child("messages")
        let messageQuery = messageRef.queryLimited(toLast:25)
        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
            let messageData = snapshot.value as! Dictionary<String, Any>
            
            if let id = messageData["senderId"] as! String!, let name = messageData["senderName"] as! String!, let text = messageData["text"] as! String!, let date = messageData["date"] as! Double!, text.characters.count > 0 {
                self.addMessage(withId: id, name: name, text: text, date: date)
                self.finishReceivingMessage()
            } else {
                print("Error! Could not decode message data")
            }
        })
    }

    private func addMessage(withId id: String, name: String, text: String, date: Double) {
        if let message = JSQMessage(senderId: id, senderDisplayName: name, date: Date(timeIntervalSince1970: date), text: text) {
            messages.append(message)
        }
    }
    
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        return bubbleFactory!.outgoingMessagesBubbleImage(with: UIColor.navy) // (bubble: UIImage.jsq_bubbleRegular(), capInsets: .zero).incomingMessagesBubbleImage(with: .clear)
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        return bubbleFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray()) // (bubble: UIImage.jsq_bubbleRegular(), capInsets: .zero).incomingMessagesBubbleImage(with: .clear)
    }
    
    @IBAction func closeViewController(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension JSQMessagesCollectionViewCellOutgoing {
    public func messageContentSmaller() {
        self.messageBubbleContainerView?.setContentCompressionResistancePriority(UILayoutPriorityDefaultLow, for: .horizontal)
        self.messageBubbleContainerView.setNeedsLayout()
    }
}

extension JSQMessagesCollectionViewCellIncoming {
    public func messageContentSmaller() {
        self.messageBubbleContainerView?.setContentCompressionResistancePriority(UILayoutPriorityDefaultLow, for: .horizontal)
        self.messageBubbleContainerView.setNeedsLayout()
    }
}
