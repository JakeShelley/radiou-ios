//
//  FavoritesViewController.swift
//  RadioU
//
//  Created by Jake on 3/18/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RealmSwift
import Moya
import SwiftyJSON

private let reuseIdentifier = "FavoriteCell"

class FavoritesViewController: UIViewController {

    let provider = MoyaProvider<RadioUEndpoints>()
    
    var tableView: UITableView!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        prepareTableView()
        self.view.addSubview(tableView)
        
        self.navigationController?.navigationBar.isHidden = true
        
        let navBar = RUNavigationBar(width: self.view.frame.width, type: .favorites)
        self.view.addSubview(navBar)
        
        let tabBar = tabBarController as! TabBarController
        tabBar.bottomPlayerIsActive.asObservable()
            .subscribe(onNext: {[weak self] _ in
                self?.prepareTableView()
            })
            .addDisposableTo(disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
        
        let realm = try! Realm()
        if (realm.objects(Show.self).count == 0) {
            setupOverlayLabel()
        } else {
            guard let overlayLabel = self.view.viewWithTag(1) else { return }
            overlayLabel.removeFromSuperview()
        }
    }
    
    private func setupTableView() {
        tableView = UITableView()
        tableView.register(UINib(nibName: "SearchTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        tableView.delegate = self
        tableView.backgroundColor = .bgGray
        tableView.separatorStyle = .none
        
        let realm = try! Realm()
        
        Observable.collection(from: realm.objects(Show.self))
            .bind(to: tableView.rx.items(cellIdentifier: reuseIdentifier, cellType: SearchTableViewCell.self)) { indexPath, show, cell in
                cell.setup(with: show)
            }
            .addDisposableTo(disposeBag)
        
        Observable.collection(from: realm.objects(Show.self))
            .subscribe(onNext: {[weak self] results in
                if (results.count == 0) {
                    self?.setupOverlayLabel()
                } else {
                    guard let overlayLabel = self?.view.viewWithTag(1) else { return }
                    overlayLabel.removeFromSuperview()
                }
            })
            .addDisposableTo(disposeBag)
        
        tableView.rx
            .itemSelected
            .map { indexPath in
                return indexPath.row
            }
            .subscribe(onNext: {[weak self] row in
                let sb = UIStoryboard(name: "Show", bundle: nil)
                let destVC = sb.instantiateInitialViewController() as! ShowViewController
                
                guard let cell = self?.tableView.cellForRow(at: [0, row]) as? SearchTableViewCell else {
                    return
                }
                
                destVC.show = cell.show
                
                self?.navigationController?.pushViewController(destVC, animated: true)
            })
            .disposed(by: disposeBag)
    }
    
    private func prepareTableView() {
        tableView.frame = TABLEVIEW_FRAME
        
        if (Player.sharedInstance.station != nil) {
            if #available(iOS 11, *) {
                if (DEVICE_MODEL == .iPhoneX) {
                    tableView.frame.size.height -= 110
                } else {
                    tableView.frame.size.height -= (BOTTOM_PLAYER_PEEK_HEIGHT*2 + 5)
                }
            } else {
                tableView.frame.size.height -= BOTTOM_PLAYER_PEEK_HEIGHT
            }
        }
        
        tableView.showsVerticalScrollIndicator = true
    }
    
    private func setupOverlayLabel() {
        // Refactor target
        if (self.view.viewWithTag(1) != nil) {
            self.view.viewWithTag(1)!.removeFromSuperview()
        }
        
        let overlayLabel = UILabel(frame: self.view.frame)
        overlayLabel.tag = 1
        overlayLabel.textColor = .white
        overlayLabel.textAlignment = .center
        overlayLabel.layer.shadowOffset = CGSize(width: 0, height: 1)
        overlayLabel.layer.shadowOpacity = 0.5
        overlayLabel.layer.shadowRadius = 4
        overlayLabel.layer.shadowColor = UIColor.gray.cgColor
        overlayLabel.font = UIFont(name: "AvenirNext-Heavy", size: 26)
        overlayLabel.numberOfLines = 0
        overlayLabel.lineBreakMode = .byWordWrapping
        overlayLabel.text = "Tap hearts to favorite shows!"
        
        self.view.addSubview(overlayLabel)
    }
    
}

extension FavoritesViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
}
