//
//  StationDetailViewController.swift
//  RadioU
//
//  Created by Jake on 3/27/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Moya
import SwiftyJSON
import RealmSwift
import Firebase

enum CellType: Int {
    case StationDetail = 0
    case Schedule = 1
    case Message = 2
    case Chat = 3
}

protocol StationDetailTableViewDelegate: class {
    func expand(type: CellType, yPos: CGFloat)
}

class StationDetailViewController: UIViewController {
    
    let provider = MoyaProvider<RadioUEndpoints>()
    let disposeBag = DisposeBag()
    let COVER_IMAGE_HEIGHT: CGFloat = 214
    
    var overlayButton: UIButton!
    var tableView: UITableView!
    var stationId: Int!
    var navBar: RUNavigationBar!
    var spinner: UIActivityIndicatorView!
    var tableViewIsUpdating: Bool = false
    fileprivate var station: Station!
    
    // Cell heights
    var scheduleCellHeight: CGFloat = SCHEDULE_CELL_HEIGHT
    var chatCellHeight: CGFloat = CLOSED_CELL_HEIGHT
    var messageCellHeight: CGFloat = CLOSED_CELL_HEIGHT
    fileprivate var MESSAGE_CELL_EXPANDED_HEIGHT: CGFloat = 200 // This can change based on message length
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .bgGray
        spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.backgroundColor = .bgGray
        spinner.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.view.addSubview(spinner)
        spinner.startAnimating()
        getStationData()
        setupTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        setupTimer()
        if (tableView != nil) {
            guard let cell = tableView.cellForRow(at: [0, 1]) as? ScheduleTableViewCell else { return }
            cell.setScheduleOffset(animated: false)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .minuteChange, object: nil)
        super.viewWillDisappear(animated)
    }
    
    func getStationData() {
        if (overlayButton != nil) {
            overlayButton.removeFromSuperview()
        }
        
        spinner.startAnimating()
        
        provider.request(.getStation(stationId: stationId), completion: {[weak self] result in
            switch result {
            case let .success(moyaResponse):
                let data = JSON(moyaResponse.data)
                let statusCode = moyaResponse.statusCode
                
                if (statusCode == 200) {
                    let station = mapStation(data: data)
                    self?.station = station
                    self?.setupView()
                } else {
                    self?.handleLoadingError(message: data["message"].stringValue)
                }
            case .failure:
                // 404/500 errors will be sent to success so this is only if the server doesn't respond
                self?.handleLoadingError(message: "Could not reach server")
            }
        })
    }

    private func setupView() {
        tableView = UITableView()
        prepareTableView()
        
        if #available(iOS 11, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        }
        
        self.view.addSubview(tableView)
        self.view.backgroundColor = .bgGray
        
        if (navBar != nil) {
            navBar.removeFromSuperview()
        }
        
        navBar = RUNavigationBar(width: self.view.frame.width, type: .detail)
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: navBar.frame.width, height: navBar.frame.height))
        titleLabel.font = UIFont(name: "AvenirNext-Heavy", size: 24)
        titleLabel.text = station.name
        titleLabel.textColor = .white
        titleLabel.textAlignment = .center
        navBar.nav.topItem?.titleView = titleLabel
        let navBarUnderline = UIView(frame: CGRect(x: 0, y: navBar.frame.height, width: navBar.frame.width, height: 0.5))
        navBarUnderline.backgroundColor = .lightGray
        navBarUnderline.isHidden = true
        self.navBar.addSubview(navBarUnderline)
        navBar.ruDelegate = self
        self.view.addSubview(navBar)
        
        let stationLabel = UILabel(frame: CGRect(x: 0, y: (COVER_IMAGE_HEIGHT - navBar.frame.height)/2, width: navBar.frame.width, height: navBar.frame.height))
        stationLabel.font = UIFont(name: "AvenirNext-Heavy", size: 24)
        stationLabel.text = station.name
        stationLabel.textColor = .white
        stationLabel.textAlignment = .center
        stationLabel.layer.shadowOpacity = 0.9
        stationLabel.layer.shadowColor = UIColor.black.cgColor
        stationLabel.layer.shadowOffset = CGSize(width: 0, height: 2)
        stationLabel.layer.shadowRadius = 2
        self.view.addSubview(stationLabel)
        
        tableView.register(UINib(nibName: "StationDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "TopCell")
        tableView.register(UINib(nibName: "ScheduleTableViewCell", bundle: nil), forCellReuseIdentifier: "ScheduleCell")
        tableView.register(UINib(nibName: "ChatTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatCell")
        tableView.register(UINib(nibName: "StationMessageViewCell", bundle: nil), forCellReuseIdentifier: "MessageCell")
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .bgGray
        tableView.separatorStyle = .none
        
        tableView.rx.contentOffset
            .asObservable()
            .bind(onNext: {[weak self] offset in
                if (self!.tableViewIsUpdating) {
                    return
                }
                
                if (offset.y <= ((self!.COVER_IMAGE_HEIGHT - self!.navBar.frame.height)/2) - 10) {
                    stationLabel.frame.origin.y = -offset.y + ((self!.COVER_IMAGE_HEIGHT - self!.navBar.frame.height)/2)
                    stationLabel.isHidden = false
                    titleLabel.isHidden = true
                } else {
                    stationLabel.isHidden = true
                    titleLabel.isHidden = false
                }
                
                if (offset.y >= self!.COVER_IMAGE_HEIGHT - self!.navBar.frame.height) {
                    if (self?.navBar.backgroundColor != .white) {
                        stationLabel.layer.shadowOpacity = 0
                        self?.navBar.nav.layer.shadowOpacity = 0
                        
                        UIView.animate(withDuration: 0.15, animations: {
                            self?.navBar.backgroundColor = .white
                            self?.navBar.nav.items?[0].leftBarButtonItem?.tintColor = .black
                            stationLabel.textColor = .black
                            navBarUnderline.isHidden = false
                            titleLabel.textColor = .black
                            UIApplication.shared.statusBarStyle = .default
                        }, completion: nil)
                    }
                } else {
                    if (self?.navBar.backgroundColor != .clear) {
                        stationLabel.layer.shadowOpacity = 0.9
                        self?.navBar.layer.shadowOpacity = 0.9
                        
                        UIView.animate(withDuration: 0.15, animations: {
                            self?.navBar.backgroundColor = .clear
                            self?.navBar.nav.items?[0].leftBarButtonItem?.tintColor = .white
                            stationLabel.textColor = .white
                            navBarUnderline.isHidden = true
                            titleLabel.textColor = .white
                            UIApplication.shared.statusBarStyle = .lightContent
                        }, completion: nil)
                    }
                }
            })
            .addDisposableTo(disposeBag)
        
        let tabBar = tabBarController as! TabBarController
        tabBar.bottomPlayerIsActive.asObservable()
            .subscribe(onNext: {[weak self] _ in
                self?.prepareTableView()
            })
            .addDisposableTo(disposeBag)
    }
    
    private func prepareTableView() {
        if (tableView != nil) {
            tableView.frame = TABLEVIEW_FRAME
            tableView.frame.origin.y = 0
            tableView.frame.size.height += 20
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
            
            if (Player.sharedInstance.station != nil) {
                tableView.frame.size.height -= BOTTOM_PLAYER_PEEK_HEIGHT
            }
            
            tableView.showsVerticalScrollIndicator = true
        }
    }
    
    private func setupTimer() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateTableViewAtMinuteChange), name: .minuteChange, object: nil)
    }
    
    func updateTableViewAtMinuteChange() {
        if (tableView != nil) {
            for cell in self.tableView.visibleCells {
                cell.performMinuteChangeUpdate()
            }
        }
    }
    
    private func handleLoadingError(message: String) {
        setupOverlayLabel()
        if (navBar != nil) {
            navBar.removeFromSuperview()
        }
        
        navBar = RUNavigationBar(width: self.view.frame.width, type: .error)
        navBar.ruDelegate = self
        self.view.addSubview(navBar)
        self.spinner.stopAnimating()
        
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.setAppearance()
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func setupOverlayLabel() {
        // Refactor target
        if (overlayButton != nil) {
            overlayButton.removeFromSuperview()
        }
        
        overlayButton = UIButton(frame: self.view.frame)
        overlayButton.frame.size.height -= 20
        overlayButton.setTitle("Tap to retry", for: .normal)
        overlayButton.setTitleColor(.white, for: .normal)
        overlayButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        overlayButton.layer.shadowOpacity = 0.5
        overlayButton.layer.shadowRadius = 4
        overlayButton.layer.shadowColor = UIColor.gray.cgColor
        overlayButton.titleLabel!.font = UIFont(name: "AvenirNext-Heavy", size: 26)!
        overlayButton.addTarget(self, action: #selector(getStationData), for: .touchUpInside)
        self.view.addSubview(overlayButton)
    }
    
}

extension StationDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath.row) {
        case 0:
            return 320
        case 1:
            return scheduleCellHeight
        case 2:
            return messageCellHeight
        case 3:
            return chatCellHeight
        default:
            return 50
        }
    }

}

extension StationDetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.row) {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TopCell") as! StationDetailTableViewCell
            cell.setupCell(with: station)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleCell") as! ScheduleTableViewCell
            cell.delegate = self
            cell.scheduleEntryDelegate = self
            cell.setupScheduleView(station: self.station)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as! StationMessageViewCell
            cell.delegate = self
            cell.setupTextView(with: station.message)
            MESSAGE_CELL_EXPANDED_HEIGHT = cell.messageTextHeight + 50 // Set the expanded height based the text message length
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell") as! ChatTableViewCell
            cell.delegate = self
            cell.chatDelegate = self
            cell.station = station
            return cell
        default:
            return UITableViewCell() // This shouldn't get hit
        }
    }
    
}

extension StationDetailViewController: RUNavigationBarDelegate {

    func leaveCurrentView() {
        self.navigationController?.popViewController(animated: true)
    }

}

extension StationDetailViewController: UIGestureRecognizerDelegate {

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

}

extension StationDetailViewController: StationDetailTableViewDelegate {
    
    func expand(type: CellType, yPos: CGFloat) {
        let yOffset = tableView.contentOffset.y
        tableViewIsUpdating = true
        var isExpanding = false
        var bottomOfCell: CGFloat = 0
        
        switch (type) {
        case .Schedule:
            if (scheduleCellHeight == SCHEDULE_CELL_HEIGHT) {
                isExpanding = true
                bottomOfCell = yPos + SCHEDULE_CELL_EXPANDED_HEIGHT
                scheduleCellHeight = SCHEDULE_CELL_EXPANDED_HEIGHT
            } else {
                scheduleCellHeight = SCHEDULE_CELL_HEIGHT
            }
        case .Chat:
            if (chatCellHeight == CLOSED_CELL_HEIGHT) {
                isExpanding = true
                bottomOfCell = yPos + CHAT_CELL_EXPANDED_HEIGHT
                chatCellHeight = CHAT_CELL_EXPANDED_HEIGHT
            } else {
                chatCellHeight = CLOSED_CELL_HEIGHT
            }
        case .Message:
            if (messageCellHeight == CLOSED_CELL_HEIGHT) {
                isExpanding = true
                bottomOfCell = yPos + MESSAGE_CELL_EXPANDED_HEIGHT
                messageCellHeight = MESSAGE_CELL_EXPANDED_HEIGHT
            } else {
                messageCellHeight = CLOSED_CELL_HEIGHT
            }
        default:
            return
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
        tableViewIsUpdating = false
        // use ypos + expanded height to figure out if bottom of screen is lower than table view
        if (yOffset + tableView.frame.height < bottomOfCell && isExpanding) {
            tableView.setContentOffset(CGPoint(x: 0, y: yOffset), animated: false) // Take table back to initial position after endupdates sets it to 0
            tableView.scrollToRow(at: [0, type.rawValue], at: .bottom, animated: true)
        }
    }

}

extension StationDetailViewController: ScheduleEntryDelegate {

    func isExpanded() -> Bool {
        if (scheduleCellHeight == SCHEDULE_CELL_EXPANDED_HEIGHT) {
            return true
        }
        
        return false
    }
    
    func transitionWith(show: Show) {
        let sb = UIStoryboard(name: "Show", bundle: nil)
        let destVC = sb.instantiateInitialViewController() as! ShowViewController
        destVC.show = show
        destVC.station = station
        destVC.transitionToStationShouldPop = true
        self.navigationController?.pushViewController(destVC, animated: true)
    }

}

extension StationDetailViewController: ChatTableViewCellDelegate {
    
    func transitionToChat() {
        let realm = try! Realm()
        if let user = realm.objects(User.self).first {
            let nav = UIStoryboard(name: "Chat", bundle: nil).instantiateInitialViewController() as! UINavigationController
            let chatVC = nav.topViewController as! ChatViewController
            chatVC.senderDisplayName = user.username
            chatVC.stationName = station.fullName
            chatVC.stationRef = Database.database().reference().child(String(station.id))
            present(nav, animated: true, completion: nil)
            return
        }
        
        let sb = UIStoryboard(name: "Login", bundle: nil)
        let loginVC = sb.instantiateInitialViewController()! as! LoginViewController
        loginVC.messageText = "Login or register to join the chat"
        loginVC.delegate = self
        self.present(loginVC, animated: true, completion: nil)
        return
    }

}

extension StationDetailViewController: LoginViewControllerDelegate {
    
    func loginShouldClose() {
        guard let loginVC = self.presentedViewController as? LoginViewController else { return }
        loginVC.dismiss(animated: true) {
            self.transitionToChat()
        }
    }
    
}
