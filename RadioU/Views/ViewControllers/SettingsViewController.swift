//
//  SettingsViewController.swift
//  RadioU
//
//  Created by Jake Shelley on 6/27/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift
import RxRealm
import UserNotifications

class SettingsViewController: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var notificationSettingsContainer: UIView!
    @IBOutlet weak var notificationSwitch: UISwitch!
    @IBOutlet weak var notificationButton: UIView!
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .bgGray
        let navBar = RUNavigationBar(width: self.view.frame.width, type: .settings)
        self.view.addSubview(navBar)
        setupButton()
        setupNotificationSettings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    func notificationSwitcherPress(_ sender: Any) {
        notificationSwitch.isUserInteractionEnabled = false
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if (settings.authorizationStatus == .denied) {
                let alert = UIAlertController(title: "Need Authorization", message: "To receive reminders when your favorite shows are playing, you'll need to authorize this app to send you local notifications in Settings.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { _ in
                    guard let appSettings = URL(string: UIApplicationOpenSettingsURLString) else { return }
                    openURL(url: appSettings)
                }))
                alert.addAction(UIAlertAction(title: "Close", style: .destructive, handler: nil))
                
                alert.setAppearance()
                self.present(alert, animated: true, completion: nil)
                self.notificationSwitch.isUserInteractionEnabled = true
                return
            }
            
            DispatchQueue.main.sync {
                self.setNotificationSettings()
            }
        }
    }
    
    private func setNotificationSettings() {
        guard var notificationsAllowed = UserDefaults.standard.value(forKey: ALLOW_SHOW_NOTIFICATIONS) as? Bool else {
            UserDefaults.standard.setValue(false, forKey: ALLOW_SHOW_NOTIFICATIONS)
            self.notificationSwitch.isUserInteractionEnabled = true
            return
        }
        
        notificationsAllowed = !notificationsAllowed
        
        if (notificationsAllowed) {
            turnOnNotifications()
        } else {
            turnOffNotifications()
        }
        
        UserDefaults.standard.setValue(notificationsAllowed, forKey: ALLOW_SHOW_NOTIFICATIONS)
        self.notificationSwitch.isUserInteractionEnabled = true
    }
    
    private func setupButton() {
        loginButton.layer.borderColor = UIColor.lightGray.cgColor
        loginButton.layer.borderWidth = 0.5
        
        let realm = try! Realm()
        Observable.collection(from: realm.objects(User.self))
            .subscribe({[weak self] _ in
                self?.setButtonTitle()
            })
            .disposed(by: disposeBag)
    }
    
    private func setButtonTitle() {
        let realm = try! Realm()
        if (realm.objects(User.self).count == 1) {
            loginButton.setTitle("Logout", for: .normal)
            loginButton.tintColor = .red
        } else {
            loginButton.setTitle("Login or Register", for: .normal)
            loginButton.tintColor = .blue
        }
    }
    
    private func setupNotificationSettings() {
        notificationSettingsContainer.layer.borderColor = UIColor.lightGray.cgColor
        notificationSettingsContainer.layer.borderWidth = 0.5
        notificationButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(notificationSwitcherPress)))
        
        _ = UserDefaults.standard.rx.observe(Bool.self, ALLOW_SHOW_NOTIFICATIONS)
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] value in
                guard value != nil else { return }
                self?.notificationSwitch.setOn(value!, animated: true)
            })
            .disposed(by: disposeBag)
        
        _ = UserDefaults.standard.rx.observe(Bool.self, NOTIFICATION_AUTHORIZATION_STATUS)
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] value in
                if (value == nil) {
                    self?.notificationSettingsContainer.isHidden = true
                } else {
                    self?.notificationSettingsContainer.isHidden = false
                }
            })
            .disposed(by: disposeBag)
        
        let realm = try! Realm()
        Observable.collection(from: realm.objects(User.self))
            .subscribe(onNext: { [weak self] userList in
                guard UserDefaults.standard.value(forKey: NOTIFICATION_AUTHORIZATION_STATUS) != nil else { return }
                if (userList.count == 0) {
                    self?.notificationSettingsContainer.isHidden = true
                } else {
                    self?.notificationSettingsContainer.isHidden = false
                }
            })
            .addDisposableTo(disposeBag)
    }
    
    private func presentWarning() {
        let alert = UIAlertController(title: "Are you sure?", message: "You won't have access to some features while logged out", preferredStyle: .alert)
        alert.setAppearance()
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
            logoutUser()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func presentLogin() {
        let sb = UIStoryboard(name: "Login", bundle: nil)
        let loginVC = sb.instantiateInitialViewController()! as! LoginViewController
        loginVC.messageText = "Login or register"
        self.present(loginVC, animated: true, completion: nil)
    }
    
    @IBAction func loginButtonPress(_ sender: Any) {
        let realm = try! Realm()
        if (realm.objects(User.self).count == 1) {
            presentWarning()
        } else {
            presentLogin()
        }
    }
    
}
