//
//  SearchTableViewController.swift
//  RadioU
//
//  Created by Jake on 3/27/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Moya
import SwiftyJSON
import RealmSwift

private let reuseIdentifier = "SearchCell"

class SearchViewController: UIViewController, UISearchControllerDelegate, UISearchBarDelegate {
    
    let provider = RxMoyaProvider<RadioUEndpoints>()
    let disposeBag = DisposeBag()
    
    fileprivate var loginVC: LoginViewController? = nil // This is a huge hack
    var tableView: UITableView!
    var searchController: UISearchController!
    var overlayLabel: UILabel!
    var spinner: UIActivityIndicatorView!
    var navBar: RUNavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = .bgGray
        
        navBar = RUNavigationBar(width: self.view.frame.width, type: .search)
        
        tableView = UITableView()
        prepareTableView()
        if #available(iOS 11, *) {
            tableView.contentInsetAdjustmentBehavior = .always
        }
        
        tableView.backgroundColor = .bgGray
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "SearchTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        tableView.delegate = self
        self.view.addSubview(tableView)
        
        let item = UINavigationItem()
        searchController = UISearchController(searchResultsController:  nil)
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.barTintColor = navBar.backgroundColor
        searchController.searchBar.backgroundImage = UIImage()
        searchController.searchBar.searchBarStyle = .prominent
        searchController.searchBar.tintColor = .lightRed
        searchController.searchBar.frame = CGRect(x: 0, y: 0, width: navBar.frame.width, height: navBar.frame.height - STATUS_BAR_HEIGHT)
        searchController.searchBar.backgroundImage = UIImage()
        searchController.searchBar.layer.borderWidth = 1
        searchController.searchBar.layer.borderColor = UIColor.navBarColor.cgColor
        searchController.searchBar.backgroundColor = .navBarColor
        item.titleView = searchController.searchBar
        navBar.nav.setItems([item], animated: false)
        
        self.definesPresentationContext = true
        self.view.addSubview(navBar)
        
        setupSpinner()
        setupRx()
        applyDismissKeyboardGesture()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchController.searchBar.becomeFirstResponder()
    }
    
    func setupRx() {
        let searchResults = searchController.searchBar.rx.text.orEmpty
            .throttle(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .flatMapLatest({[weak self] query -> Observable<[Show]> in
                if (query.isEmpty) {
                    return .just([])
                }
                
                // Refactor target
                if (self?.overlayLabel != nil) {
                    self?.overlayLabel.removeFromSuperview()
                }
                
                self?.spinner.startAnimating()
                
                return self!.search(for: query).catchErrorJustReturn([])
            })
            .observeOn(MainScheduler.instance)
        
        searchResults
            .asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: reuseIdentifier, cellType: SearchTableViewCell.self)) {
                (index, show: Show, cell) in
                cell.delegate = self
                cell.setup(with: show)
            }
            .addDisposableTo(disposeBag)
        
        searchResults
            .asObservable()
            .subscribe({[weak self] results in
                self!.spinner.stopAnimating()
                if (results.element?.count == 0) {
                    self?.setupOverlayLabel(isQuery: self?.searchController.searchBar.text != "")
                } else {
                    if (self?.overlayLabel != nil) {
                        self?.overlayLabel.removeFromSuperview()
                    }
                }
            })
            .addDisposableTo(disposeBag)
        
        tableView.rx
            .itemSelected
            .map { indexPath in
                return indexPath.row
            }
            .subscribe(onNext: {[weak self] row in
                let sb = UIStoryboard(name: "Show", bundle: nil)
                let destVC = sb.instantiateInitialViewController() as! ShowViewController
                
                guard let cell = self?.tableView.cellForRow(at: [0, row]) as? SearchTableViewCell else {
                    return
                }
                
                destVC.show = cell.show
                
                self?.navigationController?.pushViewController(destVC, animated: true)
            })
            .disposed(by: disposeBag)
        
        let tabBar = tabBarController as! TabBarController
        tabBar.bottomPlayerIsActive.asObservable()
            .subscribe(onNext: {[weak self] _ in
                self?.prepareTableView()
            })
            .addDisposableTo(disposeBag)
    }
    
    func setupOverlayLabel(isQuery: Bool) {
        // Refactor target
        if (overlayLabel != nil) {
            overlayLabel.removeFromSuperview()
        }
        
        overlayLabel = UILabel(frame: tableView.frame)
        overlayLabel.textColor = .white
        overlayLabel.textAlignment = .center
        overlayLabel.layer.shadowOffset = CGSize(width: 0, height: 1)
        overlayLabel.layer.shadowOpacity = 0.5
        overlayLabel.layer.shadowRadius = 4
        overlayLabel.layer.shadowColor = UIColor.gray.cgColor
        overlayLabel.font = UIFont(name: "AvenirNext-Heavy", size: 26)
        
        overlayLabel.text = isQuery ? "No results found" : "Start searching!"
        self.view.addSubview(overlayLabel)
    }
    
    func setupSpinner() {
        spinner = UIActivityIndicatorView(frame: CGRect(x: (self.view.frame.width-20)/2, y: (self.view.frame.height - 20)/2, width: 20, height: 20))
        spinner.activityIndicatorViewStyle = .gray
        spinner.backgroundColor = .clear
        spinner.hidesWhenStopped = true
        self.view.addSubview(spinner)
    }
    
    func search(for searchString: String) -> Observable<[Show]> {
        return provider.request(.search(searchString: searchString))
            .debug()
            .map({ response in
                if (response.statusCode == 200) {
                    return JSON(data: response.data).map({ index, showData in
                        return mapShow(data: showData)
                    })
                }
                
                return []
            })
    }
    
    func applyDismissKeyboardGesture() {
        let dismissKeyboardGesture = UISwipeGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        dismissKeyboardGesture.direction = .down
        dismissKeyboardGesture.delegate = self
        self.view.addGestureRecognizer(dismissKeyboardGesture)
    }
    
    func dismissKeyboard() {
        searchController.searchBar.endEditing(true)
    }
    
    private func prepareTableView() {
        tableView.frame = TABLEVIEW_FRAME
        if #available(iOS 11, *) {
            if (DEVICE_MODEL == .iPhoneX) {
                tableView.frame.size.height += 33
            } else {
                tableView.frame.origin.y = NAVIGATION_BAR_HEIGHT + 15
                tableView.frame.size.height -= 15
            }
        }
        
        if (Player.sharedInstance.station != nil) {
            if #available(iOS 11, *) {
                tableView.frame.size.height -= (BOTTOM_PLAYER_PEEK_HEIGHT + self.tabBarController!.tabBar.frame.height)
            } else {
                tableView.frame.size.height -= BOTTOM_PLAYER_PEEK_HEIGHT
            }
        }
        
        tableView.showsVerticalScrollIndicator = true
    }
    
}

extension SearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
}

extension SearchViewController: UIGestureRecognizerDelegate {
 
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

extension SearchViewController: LoginViewControllerDelegate {
    
    func loginShouldClose() {
        guard let loginVC = self.loginVC else { return }
        guard let showToFavorite = loginVC.showToFavorite else { return }
        loginVC.dismiss(animated: true) {
            let realm = try! Realm()
            if (realm.objects(Show.self).filter("id==\(showToFavorite.id)").count != 0) { return }
            handleFavorite(show: showToFavorite)
        }
        
        self.loginVC = nil
    }
    
}

extension SearchViewController: FavoriteDelegate {
    
    func presentLogin(with favorite: Show) {
        if (loginVC == nil) {
            let sb = UIStoryboard(name: "Login", bundle: nil)
            loginVC = sb.instantiateInitialViewController() as? LoginViewController
            loginVC!.delegate = self
            loginVC!.showToFavorite = favorite
        }
        
        self.present(loginVC!, animated: true, completion: nil)
    }
    
}
