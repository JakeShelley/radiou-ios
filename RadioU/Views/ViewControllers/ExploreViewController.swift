//
//  ExploreViewController.swift
//  RadioU
//
//  Created by Jake on 3/7/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON
import RxRealm
import RealmSwift

private let reuseIdentifier = "StationCell"

class ExploreViewController: UIViewController {

    let disposeBag = DisposeBag()
    
    let viewModel = ExploreViewModel()
    var tableView: UITableView!
    var spinner: UIActivityIndicatorView!
    var refreshControl: UIRefreshControl!
    var overlayButton: UIButton!
    var currentlyPlayingStationCellIndexPath: IndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.keyWindow?.backgroundColor = .bgGray
    
        tableView = UITableView()
        
        prepareTableView()
        self.view.addSubview(tableView)
        
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = .bgGray
        
        let navBar = RUNavigationBar(width: self.view.frame.width, type: .explore)
//        let item = navBar.items?.first!
//        navBar.items = [item!]
        self.view.addSubview(navBar)
        
        tableView.register(UINib(nibName: "StationTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .bgGray
        
        spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.frame = CGRect(x: (self.view.frame.width - 20)/2, y: (self.view.frame.height - 20)/2, width: 20, height: 20)
        spinner.hidesWhenStopped = true
        spinner.backgroundColor = .bgGray
        spinner.startAnimating()
        
        if (viewModel.stations.value.count == 0) {
            self.view.addSubview(spinner)
        }
        
        setupRx()
        setTimer()
        
        getStations(startIndex: 0, endIndex: 10)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateTableViewAtMinuteChange()
        setTimer()
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .minuteChange, object: nil)
    }
    
    func showMapView() {
        
    }
    
    func setupRx() {
        viewModel.stations.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: reuseIdentifier, cellType: StationTableViewCell.self)) { row, station, cell in
                cell.delegate = self
                cell.setupCell(station: station)
            }
            .addDisposableTo(disposeBag)
        
        tableView.rx
            .itemSelected
            .map { indexPath in
                return indexPath.row
            }
            .subscribe(onNext: {[weak self] row in
                let sb = UIStoryboard(name: "Station", bundle: nil)
                let destVC = sb.instantiateInitialViewController() as! StationDetailViewController
                destVC.stationId = self?.viewModel.stations.value[row].id
                
                self?.navigationController?.pushViewController(destVC, animated: true)
            })
            .disposed(by: disposeBag)
        
        let tabBar = tabBarController as! TabBarController
        tabBar.bottomPlayerIsActive.asObservable()
            .subscribe(onNext: {[weak self] _ in
                self?.prepareTableView()
            })
            .addDisposableTo(disposeBag)
    }
    
    func refresh(sender: Any) {
        if (overlayButton != nil) {
            overlayButton.removeFromSuperview()
        }
        
        var endIndex = viewModel.stations.value.count
        if (viewModel.stations.value.count <= 10) {
            endIndex = 10
        }
        
        viewModel.fetchStationData(startIndex: 0, endIndex: endIndex, refresh: true, completion: { status, message in
            self.spinner.stopAnimating()
            self.refreshControl.endRefreshing()
            self.tableView.isUserInteractionEnabled = true
            if (status != 200) {
                self.handleLoadingError(message: message)
            }
        })
    }
    
    func getStations(startIndex: Int, endIndex: Int) {
        if (overlayButton != nil) {
            overlayButton.removeFromSuperview()
        }
        
        viewModel.fetchStationData(startIndex: startIndex, endIndex: endIndex, refresh: false, completion: { status, message in
            self.spinner.stopAnimating()
            self.tableView.isUserInteractionEnabled = true
            if (status != 200) {
                self.handleLoadingError(message: message)
            }
        })
    }

    func prepareTableView() {
        tableView.frame = TABLEVIEW_FRAME
        
        if (Player.sharedInstance.station != nil) {
            if #available(iOS 11, *) {
                if (DEVICE_MODEL != .iPhoneX) {
                    tableView.frame.size.height -= (BOTTOM_PLAYER_PEEK_HEIGHT*2 + 10)
                }
            } else {
                tableView.frame.size.height -= BOTTOM_PLAYER_PEEK_HEIGHT
            }
        }
        
        tableView.showsVerticalScrollIndicator = true
    }

    func handleLoadingError(message: String) {
        if (viewModel.stations.value.count == 0) {
            setupOverlayLabel()
        }
        
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.setAppearance()
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setupOverlayLabel() {
        // Refactor target
        if (overlayButton != nil) {
            overlayButton.removeFromSuperview()
        }
        
        tableView.isUserInteractionEnabled = false
        overlayButton = UIButton(frame: tableView.frame)
        overlayButton.frame.size.height -= 20
        overlayButton.setTitle("Tap to retry", for: .normal)
        overlayButton.setTitleColor(.white, for: .normal)
        overlayButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        overlayButton.layer.shadowOpacity = 0.5
        overlayButton.layer.shadowRadius = 4
        overlayButton.layer.shadowColor = UIColor.gray.cgColor
        overlayButton.titleLabel!.font = UIFont(name: "AvenirNext-Heavy", size: 26)!
        overlayButton.addTarget(self, action: #selector(overlayRefresh), for: .touchUpInside)
        self.view.addSubview(overlayButton)
    }
    
    func overlayRefresh() {
        spinner.startAnimating()
        refresh(sender: self)
    }
    
    func updateTableViewAtMinuteChange() {
        if (tableView != nil) {
            for cell in tableView.visibleCells {
                cell.performMinuteChangeUpdate()
            }
        }
    }
    
    private func setTimer() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateTableViewAtMinuteChange), name: .minuteChange, object: nil)
    }
    
}

extension ExploreViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
}

extension ExploreViewController: LoginViewControllerDelegate {
    
    func loginShouldClose() {
        guard let loginVC = self.presentedViewController as? LoginViewController else { return }
        loginVC.dismiss(animated: true) {
            guard let showToFavorite = loginVC.showToFavorite else { return }
            let realm = try! Realm()
            if (realm.objects(Show.self).filter("id==\(showToFavorite.id)").count != 0) { return }
            handleFavorite(show: showToFavorite)
        }
    }
    
}

extension ExploreViewController: FavoriteDelegate {
    
    func presentLogin(with favorite: Show) {
        let sb = UIStoryboard(name: "Login", bundle: nil)
        let loginVC = sb.instantiateInitialViewController() as! LoginViewController
        loginVC.delegate = self
        loginVC.showToFavorite = favorite
        self.present(loginVC, animated: true, completion: nil)
    }
    
}
