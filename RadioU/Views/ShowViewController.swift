//
//  ShowViewController.swift
//  RadioU
//
//  Created by Jake Shelley on 5/9/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxRealm
import MarqueeLabel
import RealmSwift
import Firebase
import SwiftyJSON
import Moya

class ShowViewController: UIViewController {
    
    private let provider = MoyaProvider<RadioUEndpoints>()
    private let disposeBag = DisposeBag()
    private let LEFT_BUFFER: CGFloat = 20
    private let COVER_PHOTO_HEIGHT: CGFloat = UIScreen.main.bounds.width * 0.33 // Photo has to be 1/3 of the width
    
    var fetchingStationData = false
    var transitionToStationShouldPop = false // Decides if the transition to station should pop or push
    var show: Show!
    var station: Station!
    var navBar: RUNavigationBar!
    var scrollView: UIScrollView!
    var coverPhoto: UIImageView!
    var imageBackground: UIImageView!
    var showNameLabel: MarqueeLabel!
    var tagLabel: MarqueeLabel!
    var timeLabel: UILabel!
    var stationInfoView: UIView!
    var socialView: UIView!
    var chatIcon: UIImageView!
    var callIcon: UIImageView!
    var playButton: UIButton!
    var playLabel: UILabel!
    var activityIndicator: UIActivityIndicatorView!
    var player: Player = Player.sharedInstance // I'm going to use this in a closure. Need to check if this causes memory leaks...
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        navBar = RUNavigationBar(width: self.view.frame.width, type: .detail)
        navBar.ruDelegate = self
        navBar.backgroundColor = .clear
        navBar.layer.shadowOpacity = 0.9
        self.view.addSubview(navBar)
        
        scrollView.rx.contentOffset
            .asObservable()
            .bind(onNext: {[weak self] offset in
                if (offset.y >= self!.COVER_PHOTO_HEIGHT - NAVIGATION_BAR_HEIGHT - 20) {
                    self?.imageBackground.frame.origin.y = NAVIGATION_BAR_HEIGHT - self!.COVER_PHOTO_HEIGHT
                    self?.coverPhoto.frame.origin.y = NAVIGATION_BAR_HEIGHT - self!.COVER_PHOTO_HEIGHT
                } else {
                    self?.imageBackground.frame.origin.y = -offset.y - 20
                    self?.coverPhoto.frame.origin.y = -offset.y - 20
                }
            })
            .addDisposableTo(disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        prepareScrollView()
        NotificationCenter.default.addObserver(self, selector: #selector(updateAtMinuteChange), name: .minuteChange, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .minuteChange, object: nil)
        super.viewWillDisappear(animated)
    }
    
    func transitionToStation(gestureRecognizer: UITapGestureRecognizer) {
        if (transitionToStationShouldPop) {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        let sb = UIStoryboard(name: "Station", bundle: nil)
        let destVC = sb.instantiateInitialViewController() as! StationDetailViewController
        destVC.stationId = show.stationId

        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    func socialButtonPressed(sender: UIButton) {
        guard let url = URL(string: sender.titleLabel!.text!) else { return }
        openURL(url: url)
    }
    
    func favoriteButtonPressed() {
        let realm = try! Realm()
        if (realm.objects(User.self).first == nil) {
            let sb = UIStoryboard(name: "Login", bundle: nil)
            let loginVC = sb.instantiateInitialViewController() as! LoginViewController
            loginVC.delegate = self
            self.present(loginVC, animated: true, completion: nil)
            return
        }
        
        handleFavorite(show: show)
    }
    
    func transitionToChat() {
        let realm = try! Realm()
        if let user = realm.objects(User.self).first {
            let nav = UIStoryboard(name: "Chat", bundle: nil).instantiateInitialViewController() as! UINavigationController
            let chatVC = nav.topViewController as! ChatViewController
            chatVC.senderDisplayName = user.username
            chatVC.stationName = show.fullStationName
            chatVC.stationRef = Database.database().reference().child(String(show.stationId))
            present(nav, animated: true, completion: nil)
            return
        }
        
        let sb = UIStoryboard(name: "Login", bundle: nil)
        let loginVC = sb.instantiateInitialViewController()! as! LoginViewController
        loginVC.messageText = "Login or register to join the chat"
        self.present(loginVC, animated: true, completion: nil)
        return
    }
    
    func callStation() {
        if (show.phoneNumber == 0) {
            let alert = UIAlertController(title: "Whoops", message: "The station phone isn't available right now, try chat instead.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            alert.setAppearance()
            present(alert, animated: true, completion: nil)
            return
        }
        
        guard let url = URL(string: "tel://" + String(describing: show.phoneNumber)) else {
            return
        }
        
        openURL(url: url)
    }
    
    func updateAtMinuteChange() {
        guard tagLabel != nil || playButton != nil || playLabel != nil else { return }
        setPlayButtonImage()
        if (show.isPlaying) {
            UIView.animate(withDuration: LONG_ANIMATION_DURATION, animations: { _ in
                self.playButton.layer.borderColor = UIColor.black.cgColor
                self.playButton.imageView?.tintColor = .black
                self.playButton.isUserInteractionEnabled = true
                
                self.playLabel.text = "Live now!"
                self.callIcon.tintColor = .black
                self.callIcon.isUserInteractionEnabled = true
            })
        } else {
            UIView.animate(withDuration: LONG_ANIMATION_DURATION, animations: { _ in
                self.playButton.layer.borderColor = UIColor.lightGray.cgColor
                self.playButton.imageView?.tintColor = .lightGray
                self.playButton.isUserInteractionEnabled = false
                
                self.playLabel.text = "Show not live"
                self.callIcon.tintColor = .lightGray
                self.callIcon.isUserInteractionEnabled = false
            })
        }
    }
    
    func play() {
        guard show.isPlaying || !fetchingStationData else { return }
        let player = Player.sharedInstance
        if (station == nil) {
            if (player.station == nil) {
                loadStationForPlayer()
                setLoadingStatus(fetchingStationData: true)
                return
            }
            
            if (player.station.id != show.stationId) {
                loadStationForPlayer()
                setLoadingStatus(fetchingStationData: true)
                return
            } else {
                station = player.station
            }
        }
        
        player.playStation(station)
    }
    
    func setPlayButtonImage() {
        if (fetchingStationData) { return }
        
        let player = Player.sharedInstance
        let isPlaying = player.isPlaying.value
        if (player.station == nil) {
            playButton.setImage(#imageLiteral(resourceName: "large-play"), for: .normal)
            return
        }
        
        if (isPlaying && player.station.id == show.stationId && show.isPlaying) {
            playButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
        } else {
            playButton.setImage(#imageLiteral(resourceName: "large-play"), for: .normal)
        }
    }
    
    private func setLoadingStatus(fetchingStationData: Bool) {
        self.fetchingStationData = fetchingStationData
        if (fetchingStationData) {
            playButton.setImage(UIImage(), for: .normal)
            activityIndicator.startAnimating()
        } else {
            setPlayButtonImage()
            activityIndicator.stopAnimating()
        }
    }
    
    private func setupView() {
        scrollView = UIScrollView(frame: CGRect(x: 0, y: -STATUS_BAR_HEIGHT, width: self.view.frame.width, height: self.view.frame.height + STATUS_BAR_HEIGHT))
        
        if #available(iOS 11, *) {
            if (DEVICE_MODEL == .iPhoneX) {
                scrollView.contentInsetAdjustmentBehavior = .never
                scrollView.frame.size.height -= 65
            }
        }
        
        prepareScrollView()
        
        setupLabels()
        setupPlayButton()
        setupSocial()
        setupChat()
        setupStationInfo()
        setupMessage()
        
        var contentView = CGRect.zero
        for view in scrollView.subviews {
            contentView = contentView.union(view.frame)
        }
        
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: contentView.size.height)
        scrollView.showsVerticalScrollIndicator = false
        self.view.addSubview(scrollView)
        setupCoverPhoto()
    }
    
    private func setupCoverPhoto() {
        imageBackground = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: COVER_PHOTO_HEIGHT))
        imageBackground.backgroundColor = .bgGray
        self.view.addSubview(imageBackground)
        
        coverPhoto = UIImageView(frame: imageBackground.frame)
        
        if (show.coverPhotoPath == "") {
            // TODO - Choose random color?
            coverPhoto.backgroundColor = .navy
        } else {
            coverPhoto.sd_setImage(with: URL(string: show.coverPhotoPath), completed: {[weak self] (image, error, cacheType, url) -> Void in
                if (cacheType == .none) {
                    self?.coverPhoto.alpha = 0
                    UIView.animate(withDuration: 0.15, animations: {
                        self?.coverPhoto.alpha = 1
                    }, completion: nil)
                } else {
                    self?.coverPhoto.alpha = 1
                }
            })
        }
        
        self.view.addSubview(coverPhoto)
    }
    
    private func setupLabels() {
        let LABEL_HEIGHT: CGFloat = 25
        let IMAGE_HEIGHT: CGFloat = 28
        var yPosition: CGFloat = COVER_PHOTO_HEIGHT + 16
        showNameLabel = MarqueeLabel(frame: CGRect(x: LEFT_BUFFER + 2, y: yPosition, width: self.view.frame.width - IMAGE_HEIGHT - LEFT_BUFFER*2 - 4, height: LABEL_HEIGHT))
        showNameLabel.text = show.name
        showNameLabel.fadeLength = MARQUEE_FADE_LENGTH
        showNameLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        showNameLabel.font = UIFont(name: "AvenirNext-Medium", size: 20)
        showNameLabel.textColor = .black
        scrollView.addSubview(showNameLabel)
        
        let favoriteButton = UIButton(frame: CGRect(x: showNameLabel.frame.origin.x + showNameLabel.frame.width + 3, y: yPosition - 4, width: IMAGE_HEIGHT, height: IMAGE_HEIGHT))
        favoriteButton.setImage(#imageLiteral(resourceName: "heart-fill"), for: .normal)
        favoriteButton.addTarget(self, action: #selector(favoriteButtonPressed), for: .touchUpInside)
        scrollView.addSubview(favoriteButton)
        
        let realm = try! Realm()
        
        // TODO - Could be causing memory leak
        Observable.collection(from: realm.objects(Show.self))
            .subscribe({ _ in
                if (realm.object(ofType: Show.self, forPrimaryKey: self.show.id) != nil) {
                    favoriteButton.tintColor = .lightRed
                } else {
                    favoriteButton.tintColor = .bgGray
                }
            })
            .addDisposableTo(disposeBag)
        
        yPosition += LABEL_HEIGHT + 2
        tagLabel = MarqueeLabel(frame: showNameLabel.frame)
        tagLabel.fadeLength = MARQUEE_FADE_LENGTH
        tagLabel.frame.size.width = self.view.frame.height - LEFT_BUFFER*2 - 4
        tagLabel.frame.origin.y = yPosition
        tagLabel.text = show.tagString
        tagLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        tagLabel.font = UIFont(name: "AvenirNext-Regular", size: 15)
        tagLabel.textColor = .lightRed
        scrollView.addSubview(tagLabel)

        yPosition += LABEL_HEIGHT + 3
        timeLabel = UILabel(frame: tagLabel.frame)
        timeLabel.frame.origin.y = yPosition
        timeLabel.text = show.timeLabelString
        timeLabel.font = UIFont(name: "AvenirNext-Regular", size: 13)
        timeLabel.textColor = .lightGray
        scrollView.addSubview(timeLabel)
    }
    
    private func setupPlayButton() {
        let PLAYBUTTON_HEIGHT: CGFloat = 36
        playButton = UIButton(frame: CGRect(x: 15, y: timeLabel.frame.origin.y + timeLabel.frame.height + 6, width: PLAYBUTTON_HEIGHT, height: PLAYBUTTON_HEIGHT))
        playButton.setImage(#imageLiteral(resourceName: "large-play"), for: .normal)
        playButton.layer.cornerRadius = PLAYBUTTON_HEIGHT/2
        playButton.layer.borderWidth = 5
        playButton.addTarget(self, action: #selector(play), for: .touchUpInside)
        
        playLabel = UILabel(frame: playButton.frame)
        playLabel.frame.origin.x += PLAYBUTTON_HEIGHT + 5
        playLabel.frame.size.width = self.view.frame.width - 30 - PLAYBUTTON_HEIGHT
        playLabel.textColor = .black
        playLabel.font = UIFont(name: "AvenirNext-Medium", size: 17)
        
        if (show.isPlaying) {
            playButton.layer.borderColor = UIColor.black.cgColor
            playButton.imageView?.tintColor = .black
            playButton.isUserInteractionEnabled = true
            playLabel.text = "Live now!"
        } else {
            playButton.layer.borderColor = UIColor.lightGray.cgColor
            playButton.imageView?.tintColor = .lightGray
            self.playButton.isUserInteractionEnabled = false
            playLabel.text = "Show not live"
        }
        
        Player.sharedInstance.isPlaying
            .asObservable()
            .subscribe(onNext: {[weak self] isPlaying in
                self?.setPlayButtonImage()
            })
            .addDisposableTo(disposeBag)
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: PLAYBUTTON_HEIGHT, height: PLAYBUTTON_HEIGHT)
        activityIndicator.hidesWhenStopped = true
        playButton.addSubview(activityIndicator)
        
        scrollView.addSubview(playButton)
        scrollView.addSubview(playLabel)
    }
    
    private func setupSocial() {
        let LINK_SIZE: CGFloat = 30
        var currentXPos: CGFloat = 0
        var socialButtons = [UIButton]()

        if (show.facebook != "") {
            let fb = UIButton(frame: CGRect(x: currentXPos, y: 0, width: LINK_SIZE, height: LINK_SIZE))
            fb.setImage(#imageLiteral(resourceName: "facebook-icon"), for: .normal)
            fb.titleLabel?.text = show.facebook
            fb.addTarget(self, action: #selector(socialButtonPressed), for: .touchUpInside)
            socialButtons.append(fb)
            currentXPos += LINK_SIZE + 6
        }
    
        if (show.twitter != "") {
            let twitter = UIButton(frame: CGRect(x: currentXPos, y: 0, width: LINK_SIZE, height: LINK_SIZE))
            twitter.setImage(#imageLiteral(resourceName: "twitter-icon"), for: .normal)
            twitter.titleLabel?.text = show.twitter
            twitter.addTarget(self, action: #selector(socialButtonPressed), for: .touchUpInside)
            socialButtons.append(twitter)
            currentXPos += LINK_SIZE + 6
        }
        
        if (show.instagram != "") {
            let instagram = UIButton(frame: CGRect(x: currentXPos, y: 0, width: LINK_SIZE, height: LINK_SIZE))
            instagram.setImage(#imageLiteral(resourceName: "instagram-icon"), for: .normal)
            instagram.titleLabel?.text = show.instagram // hacky but it works
            instagram.addTarget(self, action: #selector(socialButtonPressed), for: .touchUpInside)
            socialButtons.append(instagram)
            currentXPos += LINK_SIZE + 6
        }
    
        if (show.tumblr != "") {
            let tumblr = UIButton(frame: CGRect(x: currentXPos, y: 0, width: LINK_SIZE, height: LINK_SIZE))
            tumblr.setImage(#imageLiteral(resourceName: "tumblr-icon"), for: .normal)
            tumblr.titleLabel?.text = show.tumblr
            tumblr.addTarget(self, action: #selector(socialButtonPressed), for: .touchUpInside)
            socialButtons.append(tumblr)
            currentXPos += LINK_SIZE + 6
        }
        
        if (socialButtons.count != 0) {
            socialView = UIView(frame: CGRect(x: LEFT_BUFFER - 2, y: playButton.frame.origin.y + playButton.frame.height + 14, width: self.view.frame.width, height: LINK_SIZE))
            for button in socialButtons {
                socialView.addSubview(button)
            }
            
            scrollView.addSubview(socialView)
        }
    }
    
    private func setupChat() {
        let BUFFER: CGFloat = 5
        let IMAGE_SIZE: CGFloat = 45
        
        var titleYPosition = playButton.frame.origin.y + playButton.frame.height
        if (socialView != nil) {
            titleYPosition = socialView.frame.origin.y + socialView.frame.height
        }
        
        let chatTitle = UILabel(frame: CGRect(x: LEFT_BUFFER, y: titleYPosition + BUFFER*3, width: self.view.frame.width, height: 30))
        chatTitle.text = "Chat"
        chatTitle.textColor = .lightGray
        chatTitle.font = UIFont(name: "AvenirNext-Medium", size: 22)
        scrollView.addSubview(chatTitle)
        
        chatIcon = UIImageView(image: #imageLiteral(resourceName: "chat-bubble"))
        chatIcon.frame = CGRect(x: LEFT_BUFFER, y: chatTitle.frame.origin.y + chatTitle.frame.height + 2, width: IMAGE_SIZE, height: IMAGE_SIZE)
        chatIcon.isUserInteractionEnabled = true
        let chatTap = UITapGestureRecognizer(target: self, action: #selector(transitionToChat))
        chatIcon.addGestureRecognizer(chatTap)
        scrollView.addSubview(chatIcon)
        
        callIcon = UIImageView(image: #imageLiteral(resourceName: "phone"))
        callIcon.frame = chatIcon.frame
        callIcon.frame.origin.x = chatIcon.frame.width + LEFT_BUFFER*2
        
        if (show.isPlaying) {
            callIcon.isUserInteractionEnabled = true
            callIcon.tintColor = .black
        } else {
            callIcon.isUserInteractionEnabled = false
            callIcon.tintColor = .lightGray
        }
        
        let callTap = UITapGestureRecognizer(target: self, action: #selector(callStation))
        callIcon.addGestureRecognizer(callTap)
        scrollView.addSubview(callIcon)
    }
    
    private func setupStationInfo() {
        let BUFFER: CGFloat = 5
        let ICON_SIZE: CGFloat = 48
        
        let titleYPosition = chatIcon.frame.origin.y + chatIcon.frame.height - 5
        
        let stationTitle = UILabel(frame: CGRect(x: LEFT_BUFFER, y: titleYPosition + BUFFER*3, width: self.view.frame.width, height: 30))
        stationTitle.text = "Station"
        stationTitle.textColor = .lightGray
        stationTitle.font = UIFont(name: "AvenirNext-Medium", size: 22)
        scrollView.addSubview(stationTitle)
        
        stationInfoView = UIView(frame: CGRect(x: 0, y: stationTitle.frame.origin.y + stationTitle.frame.height, width: self.view.frame.width, height: ICON_SIZE + BUFFER*2))
        
        let iconBackground = UIImageView(frame: CGRect(x: LEFT_BUFFER, y: BUFFER, width: ICON_SIZE, height: ICON_SIZE))
        iconBackground.backgroundColor = .shinyGray
        iconBackground.layer.cornerRadius = 5
        
        let stationIcon = UIImageView(frame: iconBackground.frame)
        stationIcon.layer.cornerRadius = 5
        stationIcon.clipsToBounds = true
        stationIcon.sd_setImage(with: URL(string: show.stationIconPath), completed: { (image, error, cacheType, url) -> Void in
            if (cacheType == .none) {
                stationIcon.alpha = 0
                UIView.animate(withDuration: 0.15, animations: {
                    stationIcon.alpha = 1
                }, completion: nil)
            } else {
                stationIcon.alpha = 1
            }
        })
        
        stationInfoView.addSubview(iconBackground)
        stationInfoView.addSubview(stationIcon)
        
        let fullStationNameLabel = MarqueeLabel(frame: CGRect(x: ICON_SIZE + LEFT_BUFFER + 8, y: BUFFER + 2, width: self.view.frame.width - ICON_SIZE - BUFFER*2 - LEFT_BUFFER*2, height: 20))
        fullStationNameLabel.text = show.fullStationName
        fullStationNameLabel.fadeLength = MARQUEE_FADE_LENGTH
        fullStationNameLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        fullStationNameLabel.font = UIFont(name: "AvenirNext-Medium", size: 16)
        fullStationNameLabel.textColor = .black
        stationInfoView.addSubview(fullStationNameLabel)
        
        let stationNameLabel = MarqueeLabel(frame: fullStationNameLabel.frame)
        stationNameLabel.frame.origin.y += 25
        stationNameLabel.text = show.stationName
        stationNameLabel.textColor = .lightGray
        stationNameLabel.fadeLength = MARQUEE_FADE_LENGTH
        stationNameLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        stationNameLabel.font = UIFont(name: "AvenirNext-Regular", size: 16)
        stationInfoView.addSubview(stationNameLabel)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(transitionToStation(gestureRecognizer:)))
        stationInfoView.isUserInteractionEnabled = true
        stationInfoView.addGestureRecognizer(tap)
        
        scrollView.addSubview(stationInfoView)
    }
    
    private func setupMessage() {
        let messageTitle = UILabel(frame: CGRect(x: LEFT_BUFFER, y: stationInfoView.frame.origin.y + stationInfoView.frame.height + 15, width: self.view.frame.width, height: 30))
        messageTitle.text = "Message"
        messageTitle.textColor = .lightGray
        messageTitle.font = UIFont(name: "AvenirNext-Medium", size: 22)
        scrollView.addSubview(messageTitle)

        let messageTextView = UITextView(frame: CGRect(x: LEFT_BUFFER - 4, y: messageTitle.frame.origin.y + messageTitle.frame.height - 2, width: self.view.frame.width - LEFT_BUFFER*2, height: 0))
        
        if (show.message == "") {
            messageTextView.font = UIFont(name: "AvenirNext-Italic", size: 15)
            messageTextView.text = "This show has no message"
        } else {
            messageTextView.font = UIFont(name: "AvenirNext-Regular", size: 15)
            messageTextView.text = show.message
        }
    
        messageTextView.translatesAutoresizingMaskIntoConstraints = true
        messageTextView.sizeToFit()
        messageTextView.isScrollEnabled = false
        messageTextView.isUserInteractionEnabled = false
        scrollView.addSubview(messageTextView)
    }
    
    private func prepareScrollView() {
        if (scrollView != nil) {
            if (Player.sharedInstance.station != nil) {
                if (DEVICE_MODEL == .iPhoneX) {
                    scrollView.frame.size.height -= 20
                } else {
                    scrollView.frame.size.height -= BOTTOM_PLAYER_PEEK_HEIGHT
                }
            }
        }
    }
    
    private func handleLoadingError(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.setAppearance()
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func loadStationForPlayer() {
        provider.request(.getStation(stationId: show.stationId), completion: {[weak self] result in
            switch result {
            case let .success(moyaResponse):
                let data = JSON(moyaResponse.data)
                let statusCode = moyaResponse.statusCode
                
                if (statusCode == 200) {
                    let station = mapStation(data: data)
                    self?.station = station
                    self?.player.playStation(station) // This is probably going to cause a memory leak
                } else {
                    self?.handleLoadingError(message: "Couldn't load stream data")
                }
                
                self?.setLoadingStatus(fetchingStationData: false)
            case .failure:
                self?.handleLoadingError(message: "Couldn't load stream data")
                self?.setLoadingStatus(fetchingStationData: false)
            }
        })
    }
    
}

extension ShowViewController: RUNavigationBarDelegate {

    func leaveCurrentView() {
        self.navigationController?.popViewController(animated: true)
    }

}

extension ShowViewController: LoginViewControllerDelegate {
    
    func loginShouldClose() {
        guard let loginVC = self.presentedViewController as? LoginViewController else { return }
        loginVC.dismiss(animated: true) {
            let realm = try! Realm()
            if (realm.objects(Show.self).filter("id==\(self.show.id)").count != 0) { return }
            handleFavorite(show: self.show)
        }
    }
}
