//
//  TabBarControllerViewController.swift
//  RadioU
//
//  Created by Jake on 3/6/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RealmSwift

class TabBarController: UITabBarController {

    private let disposeBag = DisposeBag()
    
    var isFullScreen: Bool = false
    var bottomPlayerView: BottomPlayerView!
    var TAB_BAR_HEIGHT: CGFloat!
    var bottomPlayerIsActive = Variable<Bool>(false)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TAB_BAR_HEIGHT = self.tabBar.bounds.height
        
        tabBar.backgroundColor = .bgGray
        tabBar.barTintColor = .white
        tabBar.tintColor = .purple
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        
        let exploreView = sb.instantiateViewController(withIdentifier: "Explore")
        setupTabBarItem(with: "list", tabBarItem: exploreView.tabBarItem, tagNumber: 0, name: "Explore")
        let searchView = sb.instantiateViewController(withIdentifier: "Search")
        setupTabBarItem(with: "search", tabBarItem: searchView.tabBarItem, tagNumber: 1, name: "Search")
        let favoritesView = sb.instantiateViewController(withIdentifier: "Favorites")
        setupTabBarItem(with: "heart", tabBarItem: favoritesView.tabBarItem, tagNumber: 2, name: "Favorites")
        let settingsView = sb.instantiateViewController(withIdentifier: "Settings")
        setupTabBarItem(with: "gear", tabBarItem: settingsView.tabBarItem, tagNumber: 3, name: "Settings")
        
        self.viewControllers = [exploreView, searchView, favoritesView, settingsView]
        self.selectedViewController = exploreView
        
        let player = Player.sharedInstance
        player.isPlaying.asObservable()
            .skip(1)
            .subscribe(onNext: {[weak self] isPlaying in
                if (player.station == nil) {
                    if (self?.bottomPlayerView != nil) {
                        self?.bottomPlayerIsActive.value = false
                        self?.bottomPlayerView.removeFromSuperview()
                    }
                    
                    self?.bottomPlayerIsActive.value = true
                    return
                }
                
                if (self?.bottomPlayerIsActive.value == false) {
                    self?.bottomPlayerIsActive.value = true
                }
                
                if (self?.bottomPlayerView == nil) {
                    self?.setupBottomBar()
                    self?.bottomPlayerView.delegate = self
                    return
                }
                
                if (self?.bottomPlayerView.station.id != player.station.id) {
                    self?.bottomPlayerView.performSetup(station: player.station)
                }
            })
            .addDisposableTo(disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (bottomPlayerView != nil) {
            bottomPlayerView.updateAtMinuteChange()
        }
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch (item.tag) {
        case 0:
            tabBar.tintColor = .purple
        case 1:
            tabBar.tintColor = .teal
        case 2:
            tabBar.tintColor = .lightRed
        default:
            tabBar.tintColor = .navy
        }
    }

    func setupTabBarItem(with imageName: String, tabBarItem: UITabBarItem, tagNumber: Int, name: String) {
        tabBarItem.tag = tagNumber
        tabBarItem.title = name
        tabBarItem.image = UIImage(named: imageName)
        tabBarItem.selectedImage = UIImage(named: imageName + "-fill")
    }
    
    func setupBottomBar() {
        bottomPlayerView = UINib(nibName: "BottomPlayerView", bundle: nil).instantiate(withOwner: self, options: nil).first as! BottomPlayerView
        bottomPlayerView.frame = self.view.frame
        bottomPlayerView.frame.origin.y = self.view.frame.height - TAB_BAR_HEIGHT - BOTTOM_PLAYER_PEEK_HEIGHT
        bottomPlayerView.performSetup(station: Player.sharedInstance.station)
        self.view.insertSubview(bottomPlayerView, belowSubview: self.tabBar)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(dragBottomPlayer))
        bottomPlayerView.addGestureRecognizer(panGesture)
        bottomPlayerView.layoutIfNeeded()
    }
    
    func dragBottomPlayer(recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.view)
        let HEIGHT_TO_TOP = self.view.frame.height - TAB_BAR_HEIGHT - BOTTOM_PLAYER_PEEK_HEIGHT
        
        if let view = recognizer.view as? BottomPlayerView {
            let percentOfTheWayToTheTop = (HEIGHT_TO_TOP - view.frame.origin.y)/HEIGHT_TO_TOP
            view.setTopBarAlpha(1 - percentOfTheWayToTheTop)
            
            if (recognizer.state == .ended) {
                var userIsPanningUp = true
                
                let velocity = recognizer.velocity(in: view)
                if (velocity.y > 0) {
                    userIsPanningUp = false
                }
            
                // If user is only three times as high as the peek height move back down
                if (view.frame.origin.y >= (self.view.frame.height - TAB_BAR_HEIGHT - BOTTOM_PLAYER_PEEK_HEIGHT) - (BOTTOM_PLAYER_PEEK_HEIGHT*3)){
                    userIsPanningUp = false
                }
                
                snapBottomPlayer(fullScreen: userIsPanningUp, view: view)
                return
            }
            
            if (recognizer.location(in: self.view).y >= self.view.frame.height - TAB_BAR_HEIGHT - BOTTOM_PLAYER_PEEK_HEIGHT ||
                recognizer.location(in: self.view).y <= 0 ||
                (view.frame.origin.y <= 0 && recognizer.velocity(in: view).y <= 0)) {
                return
            }
            
            view.center = CGPoint(x:view.center.x,
                                  y:view.center.y + translation.y)
            self.tabBar.center = CGPoint(x: self.tabBar.center.x,
                                         y: (self.view.frame.height - TAB_BAR_HEIGHT/2) + (TAB_BAR_HEIGHT * percentOfTheWayToTheTop))
        }
        
        recognizer.setTranslation(CGPoint.zero, in: self.view)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if (isFullScreen) {
            self.tabBar.center = CGPoint(x: self.tabBar.center.x,
                                         y: self.view.frame.height + self.TAB_BAR_HEIGHT)
        } else {
            self.tabBar.center.y = self.view.frame.height - (self.TAB_BAR_HEIGHT/2)
        }
        
    }
    
}

extension TabBarController: BottomPlayerDelegate {

    func snapBottomPlayer(fullScreen: Bool, view: BottomPlayerView) {
        isFullScreen = fullScreen
        if (fullScreen) {
            UIView.animate(withDuration: 0.1, animations: {
                view.frame.origin.y = 0
                self.tabBar.center = CGPoint(x: self.tabBar.center.x,
                                             y: self.view.frame.height + self.TAB_BAR_HEIGHT)
                view.setTopBarAlpha(0)
                UIApplication.shared.isStatusBarHidden = true
            })
            
            return
        }
        
        self.tabBar.center.y = self.view.frame.height - (TAB_BAR_HEIGHT/2)
        
        UIView.animate(withDuration: 0.1, animations: {
            view.frame.origin.y = self.view.frame.height - self.TAB_BAR_HEIGHT - BOTTOM_PLAYER_PEEK_HEIGHT
            self.tabBar.center.y = self.view.frame.height - (self.TAB_BAR_HEIGHT/2)
            view.setTopBarAlpha(1)
            UIApplication.shared.isStatusBarHidden = false
        })
    }
    
    
    func presentLoginView(with show: Show) {
        let sb = UIStoryboard(name: "Login", bundle: nil)
        let loginVC = sb.instantiateInitialViewController() as! LoginViewController
        loginVC.delegate = self
        loginVC.showToFavorite = show
        self.present(loginVC, animated: true, completion: nil)
    }
    
}

extension TabBarController: LoginViewControllerDelegate {

    func loginShouldClose() {
        guard let loginVC = self.presentedViewController as? LoginViewController else { return }
        guard let showToFavorite = loginVC.showToFavorite else { return }
        loginVC.dismiss(animated: true) {
            let realm = try! Realm()
            if (realm.objects(Show.self).filter("id==\(showToFavorite.id)").count != 0) { return }
            handleFavorite(show: showToFavorite)
        }
    }
    
}
