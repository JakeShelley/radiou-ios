//
//  RUNavigationBar.swift
//  RadioU
//
//  Created by Jake on 3/17/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit

enum RUNavigationBarType: String {
    case search
    case explore = "RadioU"
    case favorites = "Favorites"
    case settings = "Settings"
    case detail
    case error
}

protocol RUNavigationBarDelegate: class {
    func leaveCurrentView()
}

class RUNavigationBar: UIView {
    
    weak var ruDelegate: RUNavigationBarDelegate!
    var titleLabel: UILabel!
    var searchBar: UISearchBar!
    var underline: UIView!
    var nav: UINavigationBar!
    
    init(width: CGFloat, type: RUNavigationBarType) {
        super.init(frame: CGRect(x: 0, y: 0, width: width, height: NAVIGATION_BAR_HEIGHT))
        
        self.backgroundColor = .navBarColor
        
        switch type {
        case .explore, .favorites, .settings:
            setupDefaultBar(type: type)
        case .detail:
            setupDetail()
        case .search:
            setupSearch()
        case .error:
            setupError()
        }
    }
    
    func leaveCurrentView() {
        ruDelegate.leaveCurrentView()
    } 

    private func setupError() {
        nav = UINavigationBar(frame: CGRect(x: 0, y: STATUS_BAR_HEIGHT, width: self.frame.width, height: self.frame.height - STATUS_BAR_HEIGHT))
        let item = UINavigationItem()
        let leftButton = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: nil, action: #selector(leaveCurrentView))
        leftButton.tintColor = .black
        item.setLeftBarButton(leftButton, animated: false)
        nav.setItems([item], animated: false)
        self.addSubview(nav)
    }
    
    private func setupDefaultBar(type: RUNavigationBarType) {
        titleLabel = UILabel(frame: CGRect(x: 0, y: STATUS_BAR_HEIGHT, width: self.frame.width, height: self.frame.height - STATUS_BAR_HEIGHT))
        titleLabel.font = UIFont(name: "AvenirNext-Heavy", size: 19)
        titleLabel.textAlignment = .center
        titleLabel.text = type.rawValue
        titleLabel.textColor = .black
        self.addSubview(titleLabel)
        
        let underline = UIView(frame: CGRect(x: 0, y: self.frame.height - 0.5, width: self.frame.width, height: 0.5))
        underline.backgroundColor = .lightGray
        self.addSubview(underline)
    }
    
    private func setupDetail() {
        nav = UINavigationBar(frame: CGRect(x: 0, y: STATUS_BAR_HEIGHT, width: self.frame.width, height: self.frame.height - STATUS_BAR_HEIGHT))
        nav.layer.masksToBounds = false
        nav.layer.shadowOpacity = 0.3
        nav.layer.shadowColor = UIColor.black.cgColor
        nav.layer.shadowOffset = CGSize(width: 0, height: 0)
        nav.layer.shadowRadius = 0
        nav.setBackgroundImage(UIImage(), for: .default)
        nav.shadowImage = UIImage()
        nav.isTranslucent = true
        nav.backgroundColor = .clear
        let item = UINavigationItem()
        let leftButton = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: nil, action: #selector(leaveCurrentView))
        leftButton.tintColor = .white
        item.setLeftBarButton(leftButton, animated: false)
        let placeholderButton = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: nil, action: nil)
        placeholderButton.isEnabled = false
        placeholderButton.tintColor = .clear
        item.setRightBarButton(placeholderButton, animated: false)
        nav.setItems([item], animated: false)
        
        self.addSubview(nav)
    }

    private func setupSearch() {
        nav = UINavigationBar(frame: CGRect(x: 0, y: STATUS_BAR_HEIGHT, width: self.frame.width, height: self.frame.height - STATUS_BAR_HEIGHT))
        nav.setBackgroundImage(UIImage(), for: .default)
        nav.backgroundColor = .clear
        self.addSubview(nav)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
