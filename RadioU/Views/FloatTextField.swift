//
//  File.swift
//  RadioU
//
//  Created by Jake on 3/12/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class FloatingTextField: SkyFloatingLabelTextField {
    init(frame: CGRect, title: String, placeholder: String, color: UIColor) {
        super.init(frame: frame)
        self.title = title
        self.selectedTitle = selectedTitle
        self.placeholder = placeholder
        self.selectedTitleColor = color
        self.selectedLineColor = color
        self.titleColor = color
        self.lineColor = .bgGray
        self.errorColor = .red
        self.autocorrectionType = .no
        self.autocapitalizationType = .none
        self.font = UIFont(name: "AvenirNext", size: 20)
        self.placeholderFont = UIFont(name: "AvenirNext", size: 20)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
