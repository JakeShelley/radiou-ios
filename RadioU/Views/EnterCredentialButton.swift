//
//  EnterCredentialButton.swift
//  RadioU
//
//  Created by Jake on 3/12/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit

class EnterCredentialButton: UIButton {
    var enabledColor: UIColor!
    
    init(frame: CGRect, title: String, color: UIColor) {
        super.init(frame: frame)
        self.setTitle(title, for: .normal)
        self.titleLabel?.font = UIFont(name: "AvenirNext-Heavy", size: 15)
        self.setTitleColor(.white, for: .normal)
        self.isEnabled = false
        self.enabledColor = color
        self.layer.cornerRadius = 4
    }
    
    func setBackgroundColor(enabled: Bool) {
        var newColor: UIColor!
        if (enabled) {
            newColor = enabledColor
        } else {
            newColor = .bgGray
        }
        
        UIView.animate(withDuration: 0.1, animations: {
            self.backgroundColor = newColor
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
