//
//  PlayingAnimationView.swift
//  RadioU
//
//  Created by Jake on 3/17/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

private let DANCING_BAR_HEIGHT: CGFloat = 3
private var DANCING_BAR_VIEW_HEIGHT: CGFloat!

private class DancingBar: UIView {
    
    var isDancing = false
    
    init(width: CGFloat, position: Int) {
        let buffer: CGFloat = 1
        super.init(frame: CGRect(x: width*CGFloat(position) + buffer*CGFloat(position), y: DANCING_BAR_VIEW_HEIGHT - DANCING_BAR_HEIGHT, width: width, height: DANCING_BAR_HEIGHT))
        self.backgroundColor = .gray
    }
    
    func dance(duration: Double) {
        UIView.animate(withDuration: duration, animations: {
            self.frame.size.height = DANCING_BAR_VIEW_HEIGHT
            self.frame.origin.y = 0
        }, completion: { _ in
            UIView.animate(withDuration: duration, animations: {
                self.frame.size.height = DANCING_BAR_HEIGHT
                self.frame.origin.y = DANCING_BAR_VIEW_HEIGHT - DANCING_BAR_HEIGHT
            }, completion: { _ in
                if (self.isDancing) {
                    self.dance(duration: duration)
                } else {
                    self.layer.removeAllAnimations()
                }
            })
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// Container and handler
class DancingBarView: UIView {
    
    private let disposeBag = DisposeBag()
    
    private var bar1: DancingBar!
    private var bar2: DancingBar!
    private var bar3: DancingBar!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        DANCING_BAR_VIEW_HEIGHT = self.frame.height
        
        bar1 = DancingBar(width: self.frame.width/3, position: 1)
        bar2 = DancingBar(width: self.frame.width/3, position: 2)
        bar3 = DancingBar(width: self.frame.width/3, position: 3)
        self.addSubview(bar1)
        self.addSubview(bar2)
        self.addSubview(bar3)
        
        let player = Player.sharedInstance
        player.isPlaying.asObservable()
            .subscribe(onNext: {[weak self] isPlaying in
                self?.stopDancing()
            })
            .addDisposableTo(disposeBag)
    }
    
    func show() {
        UIView.animate(withDuration: 0.1, animations: {
            self.alpha = 1
        })
        
        dance()
    }
    
    func hide() {
        UIView.animate(withDuration: 0.1, animations: {
            self.alpha = 0
        })
        
        stopDancing()
    }
    
    func dance() {
        bar1.isDancing = true
        bar2.isDancing = true
        bar3.isDancing = true
        bar1.dance(duration: 0.3)
        bar2.dance(duration: 0.6)
        bar3.dance(duration: 0.4)
    }
    
    func stopDancing() {
        bar1.isDancing = false
        bar2.isDancing = false
        bar3.isDancing = false
    }

}
