//
//  ExploreViewModel.swift
//  RadioU
//
//  Created by Jake on 3/15/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import RxSwift
import RxCocoa
import SwiftyJSON
import Moya

struct ExploreViewModel {

    let provider = MoyaProvider<RadioUEndpoints>()
    let stations = Variable<[Station]>([])
    
    func fetchStationData(startIndex: Int, endIndex: Int, refresh: Bool, completion: @escaping (Int, String) -> ()) {
        provider.request(.getStations(startIndex: stations.value.count, endIndex: stations.value.count + 10), completion: { result in
            switch result {
            case let .success(moyaResponse):
                let data = JSON(moyaResponse.data)
                let statusCode = moyaResponse.statusCode
                
                if (statusCode == 200) {
                    if (refresh) {
                        self.stations.value.removeAll()
                    }
                    
                    for stationData in data.arrayValue {
                        self.stations.value.append(mapStation(data: stationData))
                    }
                }
                
                completion(statusCode, data["message"].stringValue)
            case .failure:
                // 404/500 errors will be sent to success so this is only if the server doesn't respond
                completion(500, "Could not reach server")
            }
        })
    }
    
}

