//
//  LoginViewModel.swift
//  RadioU
//
//  Created by Jake on 3/11/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import RxSwift
import RxCocoa
import RealmSwift
import SwiftyJSON
import Moya

struct LoginViewModel {

    let provider = MoyaProvider<RadioUEndpoints>()
    
    var registerUsername = Variable<String>("")
    var registerPassword = Variable<String>("")
    var username = Variable<String>("")
    var password = Variable<String>("")
    
    // TODO: Find out if there is a better way to do this
    
    
    // Return true if registration username/password has text
    var isRegistrationTextValid : Observable<Bool>{
        return Observable.combineLatest(registerUsername.asObservable(), registerPassword.asObservable()) { (username, password) in
            return username.characters.count > 0
                && password.characters.count > 0
        }
    }
    
    // Return true if username/password has text
    var isTextValid : Observable<Bool>{
        return Observable.combineLatest(username.asObservable(), password.asObservable()) { (username, password) in
            return username.characters.count > 0
                && password.characters.count > 0
        }
    }
    
    // Login user
    func loginUser(completion: @escaping (Int, String) -> ()) {
        provider.request(.login(username: username.value, password: password.value)) { result in
            switch result {
            case let .success(moyaResponse):
                let data = JSON(moyaResponse.data)
                let statusCode = moyaResponse.statusCode
                
                if (statusCode == 200) {
                    self.handleLogin(id: data["user"]["id"].intValue,
                                     username: data["user"]["username"].stringValue,
                                     favorites: data["user"]["favorites"].arrayValue)
                }
                
                completion(statusCode, data["message"].stringValue)
            case .failure:
                // 404/500 errors will be sent to success so this is only if the server doesn't respond
                completion(500, "Could not reach server")
            }
        }
    }
    
    // Register user
    func registerUser(completion: @escaping (Int, String) -> ()) {
        provider.request(.createUser(username: registerUsername.value, password: registerPassword.value)) { result in
            switch result {
            case let .success(moyaResponse):
                let data = JSON(moyaResponse.data)
                let statusCode = moyaResponse.statusCode
                
                if (statusCode == 201) {
                    self.handleLogin(id: data["user"]["id"].intValue,
                                     username: data["user"]["username"].stringValue,
                                     favorites: data["user"]["favorites"].arrayValue)
                }
                
                completion(statusCode, data["message"].stringValue)
            case .failure:
                // 404/500 errors will be sent to success so this is only if the server doesn't respond
                completion(500, "Could not reach server")
            }
        }
    }
    
    // Check for valid character in username/password
    func isValidCharacter(string: String) -> Bool {
        // Check for backspace
        let char = string.cString(using: .utf8)
        let isBackSpace = strcmp(char, "\\b") == -92
        
        return alphanumericSet.contains(string) || isBackSpace
    }

    // Mark: Private functions
    
    private func handleLogin(id: Int, username: String, favorites: [JSON]) {
        let realm = try! Realm()
        let user = User()
        user.id = id
        user.username = username
        
        for showData in favorites {
            let show = mapShow(data: showData)
            try! realm.write { realm.add(show) }
        }
        
        try! realm.write { realm.add(user) }
    }
}
