//
//  File.swift
//  RadioU
//
//  Created by Jake on 12/3/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import Foundation
import AVFoundation
import RxCocoa
import RxSwift

class Player: NSObject {
    
    var player: AVPlayer!
    var station: Station!
    var isPlaying = Variable<Bool>(false)
    
    static let sharedInstance: Player = {
        let sharedPlayer = Player()
        return sharedPlayer
    }()
    
    func playStation(_ station: Station) {
        if (self.station != nil) {
            if (isPlaying.value && self.station.id == station.id) {
                player.replaceCurrentItem(with: nil)
                isPlaying.value = false
                return
            }
        }
        
        self.station = station
        // Player stops working if buffer becomes full (around 1 minute of buffering)
        player = AVPlayer(url: URL(string: station.urlPath)!)
        player.play()
        isPlaying.value = true
    }
    
}
