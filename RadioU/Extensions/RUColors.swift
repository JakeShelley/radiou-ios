//
//  RUColors.swift
//  RadioU
//
//  Created by Jake on 3/7/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit

extension UIColor {
    
    class var navBarColor: UIColor {
        return UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    }
    
    class var shinyGray: UIColor {
        return UIColor(red:0.91, green:0.92, blue:0.93, alpha:1.0)
    }
    
    class var bgGray: UIColor {
        return UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0)
    }
    
    class var purple: UIColor {
        return UIColor(red:0.74, green:0.52, blue:0.84, alpha:1.0)
    }
    
    class var magenta: UIColor {
        return UIColor(red:0.61, green:0.71, blue:0.94, alpha:1.0)
    }
    
    class var lightGreen: UIColor {
        return UIColor(red:0.45, green:0.87, blue:0.65, alpha:1.0)
    }
    
    class var lightRed: UIColor {
        return UIColor(red:1.00, green:0.52, blue:0.65, alpha:1.0)
    }
    
    class var teal: UIColor {
        return UIColor(red:0.48, green:0.93, blue:0.93, alpha:1.0)
    }
    
    class var navy: UIColor {
        return UIColor(red:0.29, green:0.39, blue:0.60, alpha:1.0)
    }
    
    class var clementine: UIColor {
        return UIColor(red:0.95, green:0.73, blue:0.44, alpha:1.0)
    }
    
    class var black: UIColor {
        return UIColor(red:0.33, green:0.33, blue:0.33, alpha:1.0)
    }
    
    class var tintBlue: UIColor {
        return UIColor(red:0.16, green:0.49, blue:0.96, alpha:1.0)
    }
    
}
