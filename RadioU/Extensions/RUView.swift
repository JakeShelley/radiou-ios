//
//  RUView.swift
//  RadioU
//
//  Created by Jake on 4/26/17.
//  Copyright © 2017 Jake. All rights reserved.
//
// Source: http://stackoverflow.com/questions/33632266/animate-text-change-of-uilabel

import UIKit

extension UIView {
    
    // Usage: insert view.pushTransition right before changing content
    func pushTransition(duration:CFTimeInterval) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromTop
        animation.duration = duration
        self.layer.add(animation, forKey: kCATransitionPush)
    }
    
}
