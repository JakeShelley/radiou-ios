//
//  RUDate.swift
//  RadioU
//
//  Created by Jake on 4/5/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import Foundation

extension Date {
        
    func isBetweeen(date date1: Date, andDate date2: Date) -> Bool {
        return date1.compare(self).rawValue * self.compare(date2).rawValue >= 0
    }

}

extension DateFormatter {
    
    func getDateFormatter(hasMeridien: Bool) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.defaultDate = Date()
        if (hasMeridien) {
            formatter.dateFormat = "EEE h:mm a"
        } else {
            formatter.dateFormat = "EEE HH:mm"
        }
        
        return formatter
    }
    
}
