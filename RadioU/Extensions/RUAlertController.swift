//
//  RUAlertController.swift
//  RadioU
//
//  Created by Jake on 3/15/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit

extension UIAlertController {

    func setAppearance() {        
        let mutableTitle = NSMutableAttributedString(string: self.title!, attributes: [NSFontAttributeName: UIFont(name: "AvenirNext-Heavy", size: 20)!])
        self.setValue(mutableTitle, forKey: "attributedTitle")
        
        let mutableMessage = NSMutableAttributedString(string: self.message!, attributes: [NSFontAttributeName: UIFont(name: "AvenirNext-Medium", size: 16)!])
        self.setValue(mutableMessage, forKey: "attributedMessage")
    }
    
}
