//
//  File.swift
//  RadioU
//
//  Created by Jake Shelley on 5/9/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    static let minuteChange = Notification.Name("MinuteChange")
    
}
