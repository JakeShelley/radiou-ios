//
//  StationTableViewCell.swift
//  RadioU
//
//  Created by Jake on 3/15/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxRealm
import RealmSwift
import MarqueeLabel
import SDWebImage
import AVFoundation

protocol FavoriteDelegate: class {
    func presentLogin(with favorite: Show)
}

class StationTableViewCell: UITableViewCell {

    @IBOutlet weak var showLabel: MarqueeLabel!
    @IBOutlet weak var stationLabel: MarqueeLabel!
    @IBOutlet weak var tagLabel: MarqueeLabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playButtonDesignView: UIView!
    @IBOutlet weak var dancingBar: DancingBarView!
    
    var indexPath: IndexPath!
    var station: Station!
    var currentShow: Show!
    var favorited = false
    weak var delegate: FavoriteDelegate!
    
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        containerView.layer.cornerRadius = 2
        containerView.layer.shadowColor = UIColor.gray.cgColor
        containerView.layer.shadowOffset = CGSize(width: 1, height: 3)
        containerView.layer.shadowOpacity = 0.2
        containerView.layer.shadowRadius = 2

        playButtonDesignView.layer.cornerRadius = playButtonDesignView.frame.height/2
        playButtonDesignView.layer.borderColor = UIColor.white.cgColor
        playButtonDesignView.layer.borderWidth = 4
        
        playButton.addTarget(self, action: #selector(playOrPauseStation), for: .touchUpInside)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(playOrPauseStation))
        iconImage.addGestureRecognizer(tapGesture)
        
        self.selectionStyle = .none
    }
    
    func setupCell(station: Station) {
        let realm = try! Realm()
        self.station = station
        self.currentShow = station.currentShow
        
        stationLabel.text = station.name
        stationLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        stationLabel.fadeLength = MARQUEE_FADE_LENGTH
        stationLabel.restartLabel()
        
        showLabel.text = currentShow.name
        showLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        showLabel.fadeLength = MARQUEE_FADE_LENGTH
        showLabel.restartLabel()
        
        tagLabel.text = currentShow.tagString
        tagLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        tagLabel.fadeLength = MARQUEE_FADE_LENGTH
        tagLabel.restartLabel()
        
        iconImage.clipsToBounds = true
        
        Player.sharedInstance.isPlaying.asObservable()
            .subscribe(onNext: {[weak self] value in
                self?.setPlayButtonImage(isPlaying: value)
            })
            .addDisposableTo(disposeBag)
        
        iconImage.sd_setImage(with: URL(string: station.iconPath), completed: {[weak self] (image, error, cacheType, url) -> Void in
            if (cacheType == .none) {
                self?.iconImage.alpha = 0
                UIView.animate(withDuration: 0.15, animations: {
                    self?.iconImage.alpha = 1
                }, completion: nil)
            } else {
                self?.iconImage.alpha = 1
            }
        })
        
        if (currentShow.name == "Heavy Rotation") { // This is the name for when no show is playing
            favoriteButton.isHidden = true
            favoriteButton.isUserInteractionEnabled = false
            return
        } else {
            favoriteButton.isHidden = false
            favoriteButton.isUserInteractionEnabled = true
        }
        
        Observable.collection(from: realm.objects(Show.self))
            .subscribe({[weak self] _ in
                if (realm.object(ofType: Show.self, forPrimaryKey: self?.currentShow.id) != nil) {
                    self?.favoriteButton.tintColor = .lightRed
                } else {
                    self?.favoriteButton.tintColor = .bgGray
                }
            })
            .addDisposableTo(disposeBag)
    }
    
    func setPlayButtonImage(isPlaying: Bool) {
        let player = Player.sharedInstance
        if (player.station == nil) {
            playButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            dancingBar.hide()
            return
        }
        
        if (player.station.id == self.station.id) {
            if (isPlaying) {
                playButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                dancingBar.show()
            } else {
                playButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
                dancingBar.stopDancing()
            }
        } else {
            playButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            dancingBar.hide()
        }
    }
    
    @IBAction func selectFavorite(_ sender: Any) {
        let realm = try! Realm()
        if (realm.objects(User.self).count == 0) {
            delegate.presentLogin(with: currentShow)
        } else {
            guard let show = currentShow else { return }
            show.phoneNumber = station.phoneNumber
            show.stationId = station.id
            handleFavorite(show: show)
        }
    }
    
    func playOrPauseStation(_ sender: Any) {
        let player = Player.sharedInstance
        player.playStation(station)
    }
    
    override func performMinuteChangeUpdate() {
        if (showLabel.text != station.currentShow.name) {
            showLabel.pushTransition(duration: 0.2)
            showLabel.text = station.currentShow.name
            tagLabel.text = station.currentShow.tagString
            if (station.currentShow.name == "Heavy Rotation") {
                favoriteButton.isHidden = true
            } else {
                favoriteButton.isHidden = false
                let realm = try! Realm()
                if (realm.object(ofType: Show.self, forPrimaryKey: station.currentShow.id) != nil) {
                    favoriteButton.tintColor = .lightRed
                } else {
                    favoriteButton.tintColor = .bgGray
                }
            }
        }
    }
    
}
