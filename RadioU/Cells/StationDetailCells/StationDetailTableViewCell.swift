//
//  StationDetailTableViewCell.swift
//  RadioU
//
//  Created by Jake on 4/13/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import MarqueeLabel
import SDWebImage
import RxSwift
import RxCocoa

class StationDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var showLabel: MarqueeLabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var tagLabel: MarqueeLabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var iconContainer: UIView!
    @IBOutlet weak var socialLink: UIView!
    
    private let disposeBag = DisposeBag()
    
    weak var station: Station!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        
        iconContainer.layer.cornerRadius = 5
        iconContainer.layer.borderColor = UIColor.white.cgColor
        iconContainer.layer.borderWidth = 4
        iconContainer.layer.shadowRadius = 4
        iconContainer.layer.shadowColor = UIColor.lightGray.cgColor
        iconContainer.layer.shadowOpacity = 0.4
        iconContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        playButton.layer.borderColor = UIColor.black.cgColor
        playButton.layer.borderWidth = 5
        playButton.layer.cornerRadius = playButton.frame.height/2
    }
    
    func setupCell(with station: Station) {
        self.station = station
        
        showLabel.text = station.currentShow.name
        showLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        showLabel.fadeLength = MARQUEE_FADE_LENGTH
        showLabel.restartLabel()
        
        tagLabel.text = station.currentShow.tagString
        tagLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        tagLabel.fadeLength = MARQUEE_FADE_LENGTH
        tagLabel.restartLabel()
        
        Player.sharedInstance.isPlaying.asObservable()
            .subscribe(onNext: {[weak self] value in
                self?.setPlayButtonImage(isPlaying: value)
            })
            .addDisposableTo(disposeBag)
        
        coverImage.sd_setImage(with: URL(string: station.coverPhotoPath), completed: {[weak self] (image, error, cacheType, url) -> Void in
            if (cacheType == .none) {
                self?.coverImage.alpha = 0
                UIView.animate(withDuration: 0.15, animations: {
                    self?.coverImage.alpha = 1
                }, completion: nil)
            } else {
                self?.coverImage.alpha = 1
            }
        })
        
        iconImage.sd_setImage(with: URL(string: station.iconPath), completed: {[weak self] (image, error, cacheType, url) -> Void in
            if (cacheType == .none) {
                self?.coverImage.alpha = 0
                UIView.animate(withDuration: 0.15, animations: {
                    self?.coverImage.alpha = 1
                }, completion: nil)
            } else {
                self?.coverImage.alpha = 1
            }
        })
        
        iconImage.layer.cornerRadius = 5
        iconImage.clipsToBounds = true
        
        setupSocialLinks(station: station)
    }
    
    func socialButtonPressed(sender: UIButton) {
        guard let url = URL(string: sender.titleLabel!.text!) else { return }
        openURL(url: url)
    }
    
    func setupSocialLinks(station: Station) {
        for subview in socialLink.subviews {
            subview.removeFromSuperview()
        }

        let LINK_SIZE: CGFloat = 30
        var socialButtons = [UIButton]()
        
        if (station.facebook != "") {
            let fb = UIButton(frame: CGRect(x: 0, y: 0, width: LINK_SIZE, height: LINK_SIZE))
            fb.setImage(#imageLiteral(resourceName: "facebook-icon"), for: .normal)
            fb.titleLabel?.text = station.facebook
            fb.addTarget(self, action: #selector(socialButtonPressed), for: .touchUpInside)
            socialButtons.append(fb)
        }
    
        if (station.twitter != "") {
            let twitter = UIButton(frame: CGRect(x: 0, y: 0, width: LINK_SIZE, height: LINK_SIZE))
            twitter.setImage(#imageLiteral(resourceName: "twitter-icon"), for: .normal)
            twitter.titleLabel?.text = station.twitter
            twitter.addTarget(self, action: #selector(socialButtonPressed), for: .touchUpInside)
            socialButtons.append(twitter)
        }
    
        if (station.instagram != "") {
            let instagram = UIButton(frame: CGRect(x: 0, y: 0, width: LINK_SIZE, height: LINK_SIZE))
            instagram.setImage(#imageLiteral(resourceName: "instagram-icon"), for: .normal)
            instagram.titleLabel?.text = station.instagram // hacky but it works
            instagram.addTarget(self, action: #selector(socialButtonPressed), for: .touchUpInside)
            socialButtons.append(instagram)
        }
        
        if (station.tumblr != "") {
            let tumblr = UIButton(frame: CGRect(x: 0, y: 0, width: LINK_SIZE, height: LINK_SIZE))
            tumblr.setImage(#imageLiteral(resourceName: "tumblr-icon"), for: .normal)
            tumblr.titleLabel?.text = station.tumblr
            tumblr.addTarget(self, action: #selector(socialButtonPressed), for: .touchUpInside)
            socialButtons.append(tumblr)
        }
        
        var leftBuffer: CGFloat = 0
        
        for button in socialButtons {
            button.frame.origin.x = leftBuffer
            socialLink.addSubview(button)
            leftBuffer += LINK_SIZE + 6
        }
    }

    func setPlayButtonImage(isPlaying: Bool) {
        let player = Player.sharedInstance
        if (player.station == nil) {
            playButton.setImage(#imageLiteral(resourceName: "large-play"), for: .normal)
            return
        }
        
        if (isPlaying && player.station.id == self.station.id) {
            playButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
            
        } else {
            playButton.setImage(#imageLiteral(resourceName: "large-play"), for: .normal)
        }
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
        let player = Player.sharedInstance
        player.playStation(station)
    }
    
    override func performMinuteChangeUpdate() {
        if (showLabel.text != station.currentShow.name) {
            showLabel.pushTransition(duration: 0.2)
            showLabel.text = station.currentShow.name
            tagLabel.text = station.currentShow.tagString
        }
    }
    
}
