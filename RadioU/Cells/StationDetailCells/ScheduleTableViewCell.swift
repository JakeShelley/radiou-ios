//
//  CalendarTableViewCell.swift
//  RadioU
//
//  Created by Jake on 4/13/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var scheduleContainerView: UIView!
    @IBOutlet weak var topArrow: UIImageView!
    @IBOutlet weak var bottomArrow: UIImageView!
    
    var scheduleScrollView: UIScrollView!
    var spinner: UIActivityIndicatorView!
    var scheduleView: ScheduleView!
    var expanded = false
    
    weak var delegate: StationDetailTableViewDelegate?
    weak var scheduleEntryDelegate: ScheduleEntryDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.backgroundColor = .white
        
        scheduleScrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: SCHEDULE_CELL_EXPANDED_HEIGHT))
        scheduleContainerView.clipsToBounds = true
        scheduleContainerView.addSubview(scheduleScrollView)
        
        topView.layer.shadowColor = UIColor.lightGray.cgColor
        topView.layer.shadowOpacity = 0.3
        topView.layer.shadowOffset = CGSize(width: 2, height: 2)
        bottomView.layer.shadowColor = UIColor.lightGray.cgColor
        bottomView.layer.shadowOpacity = 0.3
        bottomView.layer.shadowOffset = CGSize(width: 2, height: -2)
        
        let topViewExpandGesture = UITapGestureRecognizer(target: self, action: #selector(expand))
        topView.addGestureRecognizer(topViewExpandGesture)
        let bottomViewExpandGesture = UITapGestureRecognizer(target: self, action: #selector(expand))
        bottomView.addGestureRecognizer(bottomViewExpandGesture)
        
        spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 255)
        spinner.startAnimating()
        self.addSubview(spinner)
    }
    
    func setScheduleOffset(animated: Bool) {
        if (expanded) {
            scheduleScrollView.contentOffset = CGPoint(x: 0, y: 0)
            flipArrows()
            return
        }
        
        let hour = Calendar.current.component(.hour, from: Date())
        
        var yOffset = ((hour-1) * 35) + 10
        if (hour <= 2) {
            yOffset = (1*35) + 10 // stick at hour 3
        } else if (hour >= 21) {
            yOffset = (20*35) + 10 // stick at hour 9
        }
        
        if (animated) {
            flipArrows()
            UIView.animate(withDuration: LONG_ANIMATION_DURATION, animations: {
                self.scheduleScrollView.contentOffset = CGPoint(x: 0, y: yOffset)
            })
        } else {
            scheduleScrollView.contentOffset = CGPoint(x: 0, y: yOffset)
        }
    }
    
    func setupScheduleView(station: Station) {
        setScheduleOffset(animated: false)
        if (scheduleView != nil) {
            return
        }
        
        scheduleView = ScheduleView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: SCHEDULE_CELL_EXPANDED_HEIGHT), station: station, scheduleEntryDelegate: scheduleEntryDelegate)
        scheduleScrollView.addSubview(scheduleView)
        spinner.stopAnimating()
    }
    
    func expand() {
        delegate!.expand(type: .Schedule, yPos: self.frame.origin.y)
        scheduleView.transitionToCurrentWeekday()
        expanded = !expanded
        setScheduleOffset(animated: true)
    }
    
    private func flipArrows() {
        var rotation: CGAffineTransform!
        if (expanded) {
            rotation = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
        } else {
            rotation = CGAffineTransform(rotationAngle: (180.0 * .pi) * 180.0)
        }
        
        UIView.animate(withDuration: LONG_ANIMATION_DURATION, animations: {
            self.topArrow.transform = rotation
            self.bottomArrow.transform = rotation
        })
    }

    override func performMinuteChangeUpdate() {
        if (!expanded) {
            setScheduleOffset(animated: true)
        }
    }
    
}
