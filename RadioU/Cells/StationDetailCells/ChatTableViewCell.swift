//
//  ChatTableViewCell.swift
//  RadioU
//
//  Created by Jake Shelley on 9/2/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit

protocol ChatTableViewCellDelegate: class {
    func transitionToChat()
}

class ChatTableViewCell: UITableViewCell {

    @IBOutlet weak var arrow: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var callImage: UIImageView!
    @IBOutlet weak var chatImage: UIImageView!
    @IBOutlet weak var chatLabel: UILabel!
    
    weak var delegate: StationDetailTableViewDelegate?
    weak var chatDelegate: ChatTableViewCellDelegate?
    weak var station: Station?
    
    var expanded: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        clipsToBounds = true
        
        topView.layer.shadowColor = UIColor.lightGray.cgColor
        topView.layer.shadowOpacity = 0.3
        topView.layer.shadowOffset = CGSize(width: 2, height: 2)
        
        let expandGesture = UITapGestureRecognizer(target: self, action: #selector(expand))
        topView.addGestureRecognizer(expandGesture)
        
        let tapToCall = UITapGestureRecognizer(target: self, action: #selector(callStation))
        callImage.addGestureRecognizer(tapToCall)
        callImage.isUserInteractionEnabled = true
        
        let tapToChat = UITapGestureRecognizer(target: self, action: #selector(openChat))
        chatImage.addGestureRecognizer(tapToChat)
        chatImage.isUserInteractionEnabled = true
    }
    
    func expand() {
        delegate!.expand(type: .Chat, yPos: self.frame.origin.y + self.frame.height)
//        flipArrow()
    }
    
    func callStation() {
        if (station!.phoneNumber == 0) {
            let alert = UIAlertController(title: "Whoops", message: "The station phone isn't available right now, try chat instead.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController?.present(alert, animated: true, completion: nil)
            return
        }
        
        guard let url = URL(string: "tel://" + String(describing: station!.phoneNumber)) else {
            return
        }
        
        openURL(url: url)
    }
    
    func openChat() {
        chatDelegate?.transitionToChat()
    }
    
    private func flipArrow() {
        var rotation: CGAffineTransform!
        if (expanded) {
            rotation = CGAffineTransform(rotationAngle: (180.0 * .pi) * 180.0)
        } else {
            rotation = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
        }
        
        expanded = !expanded
        
        UIView.animate(withDuration: LONG_ANIMATION_DURATION, animations: {
            self.arrow.transform = rotation
        })
    }
    
}
