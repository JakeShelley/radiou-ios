//
//  StationMessageViewCell.swift
//  RadioU
//
//  Created by Jake Shelley on 9/18/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit

class StationMessageViewCell: UITableViewCell {
   
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var messageArrow: UIImageView!
    
    weak var delegate: StationDetailTableViewDelegate?
    
    var messageTextHeight: CGFloat = 0
    var expanded: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        clipsToBounds = true
        
        topView.layer.shadowColor = UIColor.lightGray.cgColor
        topView.layer.shadowOpacity = 0.3
        topView.layer.shadowOffset = CGSize(width: 2, height: 2)
        
        let expandGesture = UITapGestureRecognizer(target: self, action: #selector(expand))
        topView.addGestureRecognizer(expandGesture)
    }
    
    func expand() {
        delegate!.expand(type: .Message, yPos: self.frame.origin.y + self.frame.height)
//        flipArrow()
    }
    
    func setupTextView(with message: String) {
        messageTextView.font = UIFont(name: "AvenirNext-Regular", size: 17)
        if (message == "") {
            messageTextView.text = "No station information right now, check back later!"
        } else {
            messageTextView.text = message
        }
        
        messageTextView.sizeToFit()
        messageTextView.isUserInteractionEnabled = false
        messageTextHeight = messageTextView.frame.height
    }

    private func flipArrow() {
        var rotation: CGAffineTransform!
        if (expanded) {
            rotation = CGAffineTransform(rotationAngle: (180.0 * .pi) * 180.0)
        } else {
            rotation = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
        }
        
        expanded = !expanded
        
        UIView.animate(withDuration: LONG_ANIMATION_DURATION, animations: {
            self.messageArrow.transform = rotation
        })
    }
    
}
