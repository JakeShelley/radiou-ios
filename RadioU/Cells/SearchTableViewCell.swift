//
//  SearchTableViewCell.swift
//  RadioU
//
//  Created by Jake on 4/3/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit
import MarqueeLabel
import RealmSwift
import RxSwift
import RxCocoa
import RxRealm

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var showLabel: MarqueeLabel!
    @IBOutlet weak var stationLabel: MarqueeLabel!
    @IBOutlet weak var tagLabel: MarqueeLabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    var show: Show!
    weak var delegate: FavoriteDelegate!
    
    let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 2
        containerView.layer.shadowColor = UIColor.gray.cgColor
        containerView.layer.shadowOffset = CGSize(width: 1, height: 3)
        containerView.layer.shadowOpacity = 0.2
        containerView.layer.shadowRadius = 2
        self.selectionStyle = .none
    }

    func setup(with show: Show) {
        // refactor target
        self.show = cloneShow(show)
        
        showLabel.text = show.name
        showLabel.restartLabel()
        showLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        showLabel.fadeLength = MARQUEE_FADE_LENGTH
        
        tagLabel.text = show.tagString
        tagLabel.restartLabel()
        tagLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        tagLabel.fadeLength = MARQUEE_FADE_LENGTH
        
        stationLabel.text = show.stationName
        stationLabel.restartLabel()
        stationLabel.trailingBuffer = MARQUEE_TRAILING_BUFFER
        stationLabel.fadeLength = MARQUEE_FADE_LENGTH
        
        timeLabel.text = show.timeLabelString
        
        if (self.show.isPlaying) {
            timeLabel.textColor = .lightRed
        } else {
            timeLabel.textColor = .lightGray
        }

        let realm = try! Realm()
        Observable.collection(from: realm.objects(Show.self))
            .subscribe({[weak self] _ in
                if (realm.object(ofType: Show.self, forPrimaryKey: self?.show.id) != nil) {
                    self?.favoriteButton.tintColor = .lightRed
                } else {
                    self?.favoriteButton.tintColor = .bgGray
                }
            })
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func selectFavorite(_ sender: Any) {
        let realm = try! Realm()
        if (realm.objects(User.self).count == 0) {
            delegate.presentLogin(with: show)
        } else {
            handleFavorite(show: show)
        }
    }
    
}
