//
//  File.swift
//  RadioU
//
//  Created by Jake on 3/13/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import RealmSwift

class User: Object {
    dynamic var id = 0
    dynamic var username = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
}
