//
//  Station.swift
//  RadioU
//
//  Created by Jake on 3/17/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import Foundation

class Station: NSObject {
    
    var id = 0
    var name = ""
    var fullName = ""
    var message = ""
    var urlPath = ""
    var iconPath = ""
    var coverPhotoPath = ""
    var twitter = ""
    var facebook = ""
    var instagram = ""
    var tumblr = ""
    var cymbal = ""
    var playing = false
    var phoneNumber = 0
    
    var shows = [Int: Show]()
    
    var schedule: [Int: [Int: [Int]]] = [
        1: [Int: [Int]](),
        2: [Int: [Int]](),
        3: [Int: [Int]](),
        4: [Int: [Int]](),
        5: [Int: [Int]](),
        6: [Int: [Int]](),
        7: [Int: [Int]]()
    ]
    
    var currentShow: Show {
        let components = Calendar.current.dateComponents([.weekday, .hour], from: Date())
        let showIds = getSchedule(at: components.weekday!, hour: components.hour!)
        
        if (showIds.count > 0) {
            for id in showIds {
                guard let show = shows[id] else { continue }
                if (show.isPlaying) {
                    return show
                }
            }
        }
        
        let heavyRo = Show()
        heavyRo.name = "Heavy Rotation"
        heavyRo.tagString = "#heavyro"
        return heavyRo
    }

    func getSchedule(at weekday: Int, hour: Int) -> [Int] {
        let convertedTimeComponents = convertComponentsToGMTComponents(weekday: weekday, hour: hour)
        guard let showIds = schedule[convertedTimeComponents.weekday!]![convertedTimeComponents.hour!] else {
            return []
        }
        
        return showIds
    }
    
    func addShow(_ show: Show) {
        shows[show.id] = show
        
        for time in show.showTimes {
            addShowToSchedule(startTime: time.startTime, endTime: time.endTime, id: show.id)
        }
    }

    private func addShowToSchedule(startTime: Date, endTime: Date, id: Int) {
        let calendar = Calendar.current
        let startComponents = calendar.dateComponents([.weekday, .hour], from: startTime)
        let endComponents = calendar.dateComponents([.weekday, .hour, .minute], from: endTime)
        var hours = getHourLengthOfShow(startDay: startComponents.weekday!,
                                        startHour: startComponents.hour!,
                                        endDay: endComponents.weekday!,
                                        endHour: endComponents.hour!,
                                        endMinute: endComponents.minute!)
        var currentDay = startComponents.weekday!
        var currentHour = startComponents.hour!
        while (hours > 0) {
            if (schedule[currentDay]![currentHour] == nil) {
                schedule[currentDay]![currentHour] = []
            }
            
            schedule[currentDay]![currentHour]!.append(id)
            
            // Set up for next iteration
            currentHour += 1
            hours -= 1
            if (currentHour > 23) {
                currentHour = 0
                currentDay += 1
                if (currentDay > 7) {
                    currentDay = 1
                }
            }
        }
    }
    
    // Get "true" end hours (aka actual number of hours that the show goes for)
    private func getHourLengthOfShow(startDay: Int, startHour: Int, endDay: Int, endHour: Int, endMinute: Int) -> Int {
        var hours = abs(startHour - (endHour + getDayDifference(day1: startDay, day2: endDay)))
        
        if (endMinute != 0) {
            hours += 1
        }
        
        return hours
    }
    
    private func getDayDifference(day1: Int, day2: Int) -> Int {
        var count = 0
        var day = day1
        
        while day != day2 {
            day += 1
            if (day > 7) {
                day = 1
            }
            
            count += 1
        }
        
        return count * 24
    }
    
    // Takes components for weekday, hour, and minute and converts them to the current TimeZone
    private func convertComponentsToGMTComponents(weekday: Int, hour: Int) -> DateComponents {
        let dateString = DAY_NAMES[weekday]! + " " + String(describing: hour) + ":00"
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "E HH:mm"
        dateFormatter.defaultDate = Date()
        let date = dateFormatter.date(from: dateString)
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "GMT")!
        return calendar.dateComponents([.weekday, .hour], from: date!)
    }
    
}
