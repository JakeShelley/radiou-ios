//
//  Time.swift
//  RadioU
//
//  Created by Jake Shelley on 5/30/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import Foundation
import RealmSwift

class Time: Object {
    
    dynamic var startTimeString = ""
    dynamic var endTimeString = ""
 
    var startTime: Date {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "EEE h:mm a"
        formatter.defaultDate = Date()
        return formatter.date(from: startTimeString)!
    }
    
    var endTime: Date {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "EEE h:mm a"
        formatter.defaultDate = Date()
        return formatter.date(from: endTimeString)!
    }
    
    var localizedStartTime: Date {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "EEE h:mm a"
        formatter.defaultDate = Date()
        formatter.timeZone = TimeZone(secondsFromGMT: 0)!
        return formatter.date(from: startTimeString)!
    }
    
    var localizedEndTime: Date {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "EEE h:mm a"
        formatter.defaultDate = Date()
        formatter.timeZone = TimeZone(secondsFromGMT: 0)!
        return formatter.date(from: endTimeString)!
    }
    
    var localizedStartTimeString: String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "EEE h:mm a"
        formatter.defaultDate = Date()
        formatter.timeZone = TimeZone.current
        return formatter.string(from: localizedStartTime)
    }

    var localizedEndTimeString: String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "EEE h:mm a"
        formatter.defaultDate = Date()
        formatter.timeZone = TimeZone.current
        return formatter.string(from: localizedEndTime)
    }
    
    var dayString: String {
        return localizedStartTimeString.components(separatedBy: " ")[0]
    }
    
    var readableStartTime: String {
        let components = localizedStartTimeString.components(separatedBy: " ")
        return components[1] + components[2]
    }
    
    var readableEndTime: String {
        let components = localizedEndTimeString.components(separatedBy: " ")
        return components[1] + components[2]
    }
    
    var timeLabelString: String {
        return dayString + " " + readableStartTime + " - " + readableEndTime
    }

    func contains(weekday: Int, hour: Int) -> Bool {
        let startComponents = Calendar.current.dateComponents([.hour, .weekday], from: localizedStartTime)
        var currentWeekday = startComponents.weekday!
        let endComponents = Calendar.current.dateComponents([.hour, .weekday], from: localizedEndTime)
        while (currentWeekday != weekday) {
            if (currentWeekday == endComponents.weekday!) {
                return false
            }
            
            currentWeekday += 1
            if (currentWeekday > 7) { // Loop at Saturday
                currentWeekday = 1
            }
        }
        
        var startHour = 0
        var endHour = 23
        if (currentWeekday == startComponents.weekday!) {
            startHour = startComponents.hour!
        }
        
        if (currentWeekday == endComponents.weekday!) {
            endHour = endComponents.hour!
        }
        
        if (hour <= endHour && hour >= startHour) {
            return true
        }
        
        return false
    }
    
}
