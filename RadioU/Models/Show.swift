//
//  Show.swift
//  RadioU
//
//  Created by Jake on 3/20/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import RealmSwift
import Foundation

class Show: Object {

    dynamic var id = 0
    dynamic var name = ""
    dynamic var message = ""
    dynamic var tagString = ""
    dynamic var active = true
    dynamic var coverPhotoPath = ""
    dynamic var facebook = ""
    dynamic var twitter = ""
    dynamic var instagram = ""
    dynamic var tumblr = ""
    dynamic var stationId = 0
    dynamic var stationName = ""
    dynamic var fullStationName = ""
    dynamic var stationIconPath = ""
    dynamic var phoneNumber = 0
    
    var showTimes = List<Time>()
    
    var timeLabelString: String {
        if (name == "" || name == "Heavy Rotation") {
            return ""
        }
        
        if (showTimes.count == 1) {
            return showTimes[0].timeLabelString
        }

        var timeLabelString: String = ""
        var days: [String] = []
        
        for time in showTimes {
            days.append(time.dayString)
        }
        
        for day in sortByDayOfWeek(days) {
            timeLabelString += day + ", "
        }
        
        return timeLabelString.substring(to: timeLabelString.index(timeLabelString.endIndex, offsetBy: -2))
    }
    
    var startTimes: [Date] {
        return showTimes.map {
            $0.startTime
        }
    }
    
    var endTimes: [Date] {
        return showTimes.map {
            $0.endTime
        }
    }
    
    var isPlaying: Bool {
        if (showTimes.count == 0) {
            return false
        }
        
        var currentTimeComponents = Calendar.current.dateComponents([.weekday, .hour, .minute], from: Date())
        
        for showTime in showTimes {
            if (showTime.contains(weekday: currentTimeComponents.weekday!, hour: currentTimeComponents.hour!)) {
                var startMinutes = Calendar.current.component(.minute, from: showTime.localizedStartTime)
                var endMinutes = Calendar.current.component(.minute, from: showTime.localizedEndTime)
                
                let startDay = Calendar.current.component(.weekday, from: showTime.localizedStartTime)
                let endDay = Calendar.current.component(.weekday, from: showTime.localizedEndTime)
                
                let startHour = Calendar.current.component(.hour, from: showTime.localizedStartTime)
                let endHour = Calendar.current.component(.hour, from: showTime.localizedEndTime)
                
                
                if (currentTimeComponents.weekday! != startDay ||
                    currentTimeComponents.hour != startHour) {
                    startMinutes = 0
                }
                
                if (currentTimeComponents.weekday! != endDay ||
                    currentTimeComponents.hour != endHour) {
                    endMinutes = 59
                }
                
                if (currentTimeComponents.minute! >= startMinutes && currentTimeComponents.minute! <= endMinutes) {
                    return true
                }
                
                return false
            }
        }
        
        return false
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
}
