//
//  AppDelegate.swift
//  RadioU
//
//  Created by Jake on 12/3/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import UIKit
import RealmSwift
import AVFoundation
import MediaPlayer
import SDWebImage
import Firebase
import UserNotifications
import RxSwift
import RxCocoa

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    private let notificationKey = "MinuteChange"
    
    var window: UIWindow?
    var timeTillTopOfMinuteTimer: Timer!
    var minuteTimer: Timer!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.keyWindow?.backgroundColor = .bgGray
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        FirebaseApp.configure()
        setupTimer()
        
        let commandCenter = MPRemoteCommandCenter.shared()
        commandCenter.pauseCommand.isEnabled = true
        commandCenter.pauseCommand.addTarget(self, action: #selector(pauseCommand))
        commandCenter.playCommand.isEnabled = true
        commandCenter.playCommand.addTarget(self, action: #selector(playCommand))
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                print("AVAudioSession is Active")
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
        
        if (UserDefaults.standard.value(forKey: NOTIFICATION_AUTHORIZATION_STATUS) != nil) {
            didUserChangeAuthorizationStatus()
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {}
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        if (Player.sharedInstance.player == nil) { // Nothing is playing
            invalidateTimers()
            return
        }
        
        updateNowPlayingInfo()
        NotificationCenter.default.addObserver(self, selector: #selector(updateNowPlayingInfo), name: .minuteChange, object: self)
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        invalidateTimers()
        NotificationCenter.default.removeObserver(self, name: .minuteChange, object: self)
        setupTimer()
        if (UserDefaults.standard.value(forKey: NOTIFICATION_AUTHORIZATION_STATUS) != nil) {
            didUserChangeAuthorizationStatus()
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationCenter.default.post(name: .minuteChange, object: self)
    }

    func applicationWillTerminate(_ application: UIApplication) {}
    
    func playCommand() {
        Player.sharedInstance.playStation(Player.sharedInstance.station)
    }
    
    func pauseCommand() {
        Player.sharedInstance.playStation(Player.sharedInstance.station)
    }
    
    func updateNowPlayingInfo() {
        guard let station = Player.sharedInstance.station else {
            return
        }
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [
            MPMediaItemPropertyTitle: station.currentShow.name,
            MPMediaItemPropertyArtist: station.name
        ]
    }
    
    private func setupTimer() {
        let currentSeconds = Calendar.current.component(.second, from: Date())
        let timeTillTopOfTheMinute = Double(60 - currentSeconds)
        
        timeTillTopOfMinuteTimer = Timer.scheduledTimer(withTimeInterval: timeTillTopOfTheMinute, repeats: false, block: {[weak self] _ in
            NotificationCenter.default.post(name: Notification.Name(rawValue: self!.notificationKey), object: self)
            self?.minuteTimer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true, block: { _ in
                NotificationCenter.default.post(name: .minuteChange, object: self)
            })
        })
    }
    
    private func invalidateTimers() {
        if (timeTillTopOfMinuteTimer != nil) {
            timeTillTopOfMinuteTimer.invalidate()
        }
        
        if (minuteTimer != nil) {
            minuteTimer.invalidate()
        }
    }
    
    private func didUserChangeAuthorizationStatus() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            let currentAuthStatus = settings.authorizationStatus.rawValue - 1 // This converts it to raw bool value...
            let oldAuthStatus = UserDefaults.standard.value(forKey: NOTIFICATION_AUTHORIZATION_STATUS) as? Int ?? currentAuthStatus
            DispatchQueue.main.sync {
                if (currentAuthStatus != oldAuthStatus) {
                    UserDefaults.standard.setValue(currentAuthStatus, forKey: ALLOW_SHOW_NOTIFICATIONS)
                    UserDefaults.standard.setValue(currentAuthStatus, forKey: NOTIFICATION_AUTHORIZATION_STATUS)
                    if (currentAuthStatus == 1) {
                        turnOnNotifications()
                    }
                }
            }
        }
    }

}
