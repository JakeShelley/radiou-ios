//
//  Radio.swift
//  RadioU
//
//  Created by Jake on 3/13/17.
//  Copyright © 2017 Jake. All rights reserved.
//
// https://github.com/Moya/Moya/blob/master/docs/Examples/Basic.md

import Moya

enum RadioUEndpoints {
    case login(username: String, password: String)
    case createUser(username: String, password: String)
    case getStations(startIndex: Int, endIndex: Int)
    case getStation(stationId: Int)
    case search(searchString: String)
    case favorite(userId: Int, showId: Int, favorite: Bool)
    case getFavorites(userId: Int)
}

extension RadioUEndpoints: TargetType {
    var baseURL: URL { return URL(string: "http://www.radiouapp.com/api/v0.1")! }
    var path: String {
        switch self {
        case .getStation(_):
            return "/station"
        case .getStations(_, _):
            return "/stations"
        case .login(_, _):
            return "/user"
        case .createUser(_, _):
            return "/user/register"
        case .search(_):
            return "/search"
        case .favorite(_, _, _):
            return "/show/favorites/change"
        case .getFavorites(_):
            return "/show/favorites"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getStations, .search, .getFavorites, .getStation:
            return .get
        case .login, .createUser, .favorite:
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .getStation(let stationId):
            return ["stationId": stationId]
        case .getStations(let startIndex, let endIndex):
            return ["startIndex": startIndex, "endIndex": endIndex]
        case .login(let username, let password), .createUser(let username, let password):
            return ["username": username, "password": password]
        case .search(let searchString):
            return ["searchString": searchString]
        case .favorite(let userId, let showId, let favorite):
            return ["userId": userId, "showId": showId, "favorite": favorite]
        case .getFavorites(let userId):
            return ["userId": userId]
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .getStations, .search, .getFavorites, .getStation:
            return URLEncoding.default
        default:
            return JSONEncoding.default
        }
    }
    
    var task: Task {
        switch self {
        case .login, .createUser, .getStations, .search, .favorite, .getFavorites, .getStation:
            return .request
        }
    }
    
    // Provides stub data for use in testing.
    var sampleData: Data {
        switch self {
        case .login(let username), .createUser(let username):
            return "{\"id\": 100, \"username\": \"\(username)\"}".utf8Encoded
        case .getStations(let startIndex):
            return String(describing: startIndex).utf8Encoded // Not sure what sample response of JSON array would look like
        case .search(let searchString):
            return String(searchString).utf8Encoded
        case .favorite(let success):
            return String(describing: success).utf8Encoded
        case .getFavorites(let success):
            return String(describing: success).utf8Encoded
        case .getStation(let success):
            return String(describing: success).utf8Encoded
        }
    }
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
}
