//
//  Mappers.swift
//  RadioU
//
//  Created by Jake Shelley on 6/17/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

func mapShow(data: JSON) -> Show {
    let show = Show()
    show.id = data["id"].intValue
    show.name = data["name"].stringValue
    show.tagString = data["tagString"].stringValue
    show.coverPhotoPath = data["cover_photo_path"].stringValue
    show.message = data["message"].stringValue
    show.facebook = data["facebook"].stringValue
    show.twitter = data["twitter"].stringValue
    show.instagram = data["instagram"].stringValue
    show.tumblr = data["tumblr"].stringValue
    show.stationIconPath = data["station_icon_path"].stringValue
    show.stationName = data["station_name"].stringValue
    show.stationId = data["station_id"].intValue
    show.fullStationName = data["station_full_name"].stringValue
    show.phoneNumber = data["phone"].intValue
    show.active = data["active"].boolValue
    
    for showTime in data["show_times"].arrayValue {
        let time = Time()
        time.startTimeString = showTime[0].stringValue
        time.endTimeString = showTime[1].stringValue
        show.showTimes.append(time)
    }
    
    // If show is a favorite, update it
    let realm = try! Realm()
    if (realm.object(ofType: Show.self, forPrimaryKey: show.id) != nil) {
        updateFavorited(show: show)
    }
    
    return show
}

func mapStation(data: JSON) -> Station {
    let station = Station()
    station.id = data["id"].intValue
    station.name = data["name"].stringValue
    station.fullName = data["full_name"].stringValue
    station.urlPath = data["url_path"].stringValue
    station.iconPath = data["icon_path"].stringValue
    station.coverPhotoPath = data["cover_photo_path"].stringValue
    station.twitter = data["twitter"].stringValue
    station.facebook = data["facebook"].stringValue
    station.instagram = data["instagram"].stringValue
    station.tumblr = data["tumblr"].stringValue
    station.message = data["message"].stringValue
    station.phoneNumber = data["phone"].intValue
    
    for showData in data["shows"].arrayValue {
        let show = mapShow(data: showData)
        show.stationIconPath = station.iconPath
        show.stationName = station.name
        show.fullStationName = station.fullName
        show.phoneNumber = station.phoneNumber
        show.stationId = station.id
        station.addShow(show)
        
        let realm = try! Realm()
        if (realm.object(ofType: Show.self, forPrimaryKey: show.id) != nil) {
            updateFavorited(show: show)
        }
    }
    
    return station
}
