//
//  Helpers.swift
//  RadioU
//
//  Created by Jake on 3/12/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import Foundation
import RealmSwift
import Moya
import SwiftyJSON
import UserNotifications

// Alphanumeric characters allowed in username/password
let alphanumericSet: Set<String> = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "_", ".", "@", "&"]

// Set or Remove favorite and return if it is now favorited or not
func handleFavorite(show: Show) {
    let realm = try! Realm()
    
    guard let user = realm.objects(User.self).first else { return } // This should never get hit :)
    
    var favorite: Bool!
    
    if (realm.object(ofType: Show.self, forPrimaryKey: show.id) != nil) {
        try! realm.write { realm.delete(realm.object(ofType: Show.self, forPrimaryKey: show.id)!) }
        favorite = false
        removeNotifications(for: show)
    } else {
        let realmShow = cloneShow(show)
        try! realm.write { realm.add(realmShow) }
        favorite = true
        addNotifications(for: show)
    }
    
    let moyaProvider = MoyaProvider<RadioUEndpoints>()
    moyaProvider.request(.favorite(userId: user.id,
                                   showId: show.id,
                                   favorite: favorite),
                         completion: { result in
                            switch result {
                            case let .success(moyaResponse):
                                if (moyaResponse.statusCode == 200) {
                                    return
                                }
                            case .failure:
    
                                break
                            }
    })
    
    if (UserDefaults.standard.value(forKey: NOTIFICATION_AUTHORIZATION_STATUS) == nil) {
        let alert = UIAlertController(title: "Favorites", message: "Get notified when your favorite shows are playing! Enable/disable notifications in settings.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Got it!", style: .default, handler: { _ in
            registerForLocalNotifications()
        }))
        
        alert.setAppearance()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
}

func cloneShow(_ show: Show) -> Show {
    let newShow = Show()
    newShow.id = show.id
    newShow.active = show.active
    newShow.coverPhotoPath = show.coverPhotoPath
    newShow.stationId = show.stationId
    newShow.facebook = show.facebook
    newShow.tagString = show.tagString
    newShow.message = show.message
    newShow.twitter = show.twitter
    newShow.instagram = show.instagram
    newShow.stationId = show.stationId
    newShow.fullStationName = show.fullStationName
    newShow.name = show.name
    newShow.stationIconPath = show.stationIconPath
    newShow.stationName = show.stationName
    newShow.phoneNumber = show.phoneNumber
    
    for showTime in show.showTimes {
        let time = Time()
        time.startTimeString = showTime.startTimeString
        time.endTimeString = showTime.endTimeString
        newShow.showTimes.append(time)
    }
    
    return newShow
}

// Sort days of the week
func sortByDayOfWeek(_ days: [String]) -> [String] {
    let daysOfWeek = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"].filter({ item in
        return days.contains(item)
    })
    
    return daysOfWeek
}

// Open url
func openURL(url: URL) {
    if #available(iOS 10.0, *) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    } else {
        UIApplication.shared.openURL(url)
    }
}

// Create local notification
private func addNotifications(for show: Show) {
    let content = UNMutableNotificationContent()
    content.title = "\(show.name) is playing now!"
    content.body = "Start listening on RadioU"
    
    for showTime in show.showTimes {
        var dateComponents = Calendar.current.dateComponents([.weekday, .minute, .hour], from: showTime.startTime)
        dateComponents.timeZone = TimeZone(abbreviation: "GMT")!
        let identifier = "\(show.id)\(dateComponents.weekday!)\(dateComponents.hour!)\(dateComponents.minute!)"
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}

private func removeNotifications(for show: Show) {
    let identifiers = show.showTimes.map { showTime -> String in
        var dateComponents = Calendar.current.dateComponents([.weekday, .minute, .hour], from: showTime.startTime)
        dateComponents.timeZone = TimeZone(abbreviation: "GMT")!
        return "\(show.id)\(dateComponents.weekday!)\(dateComponents.hour!)\(dateComponents.minute!)"
    } as [String]
    
    UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifiers)
}

func turnOnNotifications() {
    let realm = try! Realm()
    for show in realm.objects(Show.self) {
        addNotifications(for: show)
    }
}

func turnOffNotifications() {
    UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
}

func logoutUser() {
    let realm = try! Realm()
    try! realm.write { realm.deleteAll() }
    UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
}

// Update favorited show
func updateFavorited(show: Show) {
    if (show.stationName == "") { return } // Station Data did not come in
    
    let realm = try! Realm()
    removeNotifications(for: realm.object(ofType: Show.self, forPrimaryKey: show.id)!)
    
    let realmShow = cloneShow(show)
    try! realm.write { realm.add(realmShow, update: true) }
    addNotifications(for: show)
}

func registerForLocalNotifications() {
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {
        (granted, error) in
        if (granted) {
            UserDefaults.standard.setValue(true, forKey: ALLOW_SHOW_NOTIFICATIONS)
            UserDefaults.standard.setValue(true, forKey: NOTIFICATION_AUTHORIZATION_STATUS)
        } else {
            UserDefaults.standard.setValue(false, forKey: ALLOW_SHOW_NOTIFICATIONS)
            UserDefaults.standard.setValue(false, forKey: NOTIFICATION_AUTHORIZATION_STATUS)
        }
    }
}
