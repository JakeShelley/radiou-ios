//
//  Constants.swift
//  RadioU
//
//  Created by Jake on 4/19/17.
//  Copyright © 2017 Jake. All rights reserved.
//

import UIKit

// User Default Keys
let ALLOW_SHOW_NOTIFICATIONS: String = "allowShowNotifications"
let NOTIFICATION_AUTHORIZATION_STATUS: String = "notificationAuthorizationStatus"

// Buffers/Frames
let MARQUEE_TRAILING_BUFFER: CGFloat = 30
let MARQUEE_FADE_LENGTH: CGFloat = 3
let NAVIGATION_BAR_HEIGHT: CGFloat = 64
// how much the bottomplayer peeks above the tab bar
let BOTTOM_PLAYER_PEEK_HEIGHT: CGFloat = {
    // Hacky fix for iphone X
    if (DEVICE_MODEL == .simulator) {
        return 60
    } else {
        return 45
    }
}()
let STATUS_BAR_HEIGHT: CGFloat = 20
var TABLEVIEW_FRAME: CGRect {
    if #available(iOS 11, *) {
        return CGRect(x: 0, y: NAVIGATION_BAR_HEIGHT, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - NAVIGATION_BAR_HEIGHT)
    }

    return CGRect(x: 0, y: NAVIGATION_BAR_HEIGHT - 20, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - NAVIGATION_BAR_HEIGHT + 10)
}

// Station Detail cell heights
let SCHEDULE_CELL_HEIGHT: CGFloat = 255
let SCHEDULE_CELL_EXPANDED_HEIGHT: CGFloat = 965
let CLOSED_CELL_HEIGHT: CGFloat = 50
let CHAT_CELL_EXPANDED_HEIGHT: CGFloat = 220

let LONG_ANIMATION_DURATION = 0.3
let SHORT_ANIMATION_DURATOIN = 0.15

var DAY_NAMES: [Int: String] = [
    1: "Sun",
    2: "Mon",
    3: "Tue",
    4: "Wed",
    5: "Thu",
    6: "Fri",
    7: "Sat",
]

let DEVICE_MODEL = UIDevice().type
